#include "stdafx.h"


#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "time.h"
#include "windows.h"

#include "GL/glut.h"

#define BLACK 0.2, 0.2, 0.2

double cameraFromX,cameraFromY,cameraFromZ;

double cameraAngle;

int center_camera;

int rotate_camera;

int camera_pause;

int showWall;

double curtain_count;

double max_curtain = 20;
double min_curtain = 6;

double curtain_open;

void drawLamp() {
	
	float bottom_height = 6;
	float bottom_stand_height =20;
	float middle_sphere_radius = 3;
	float top_stand_height =20;
	float top_light_height = 14;

	float top_light_bottom = 14;
	float top_light_top= 8;
	float bottom_base = 8;

	float stand_width = 1;

		glPushMatrix();{
		GLUquadricObj *quadObj = gluNewQuadric();

		//bottom
		glColor3ub(130,100,60);
		gluCylinder(quadObj, bottom_base, stand_width, bottom_height, 20, 20);

		//bottom stand
		glTranslatef(0,0,bottom_height);
		glColor3ub(130,100,60);
		gluCylinder(quadObj, stand_width, stand_width, bottom_stand_height, 20, 20);

		//middle sphere
		glTranslatef(0,0,bottom_stand_height+middle_sphere_radius-2);
		glColor3ub(236,232,130);
		glutSolidSphere(middle_sphere_radius,20,20);
	
		//top stand
		glTranslatef(0,0,middle_sphere_radius-2);
		glColor3ub(236,232,130);
		gluCylinder(quadObj, stand_width, stand_width, top_stand_height, 20, 20);
		
		//top
		glTranslatef(0,0,top_stand_height);
		glColor3ub(130,100,60);
		gluCylinder(quadObj, top_light_bottom, top_light_top, top_light_height, 20, 20); //gluCylinder(quadObj, base, top, height, slices, stacks)
		
		//top disk
		//glTranslatef(0,0,30);//30 top er height
		//gluDisk(quadObj, 0, 10, 40, 40); //gluDisk(quadObj, innerRadius, outerRadius, slices, loops)
		
		

	}glPopMatrix();
}

void drawCurvedLegs(float length, float base, float top, int slices) {

	float radius = (length/2)/(sinf(3.1415926f/4)); //r = (l/2)/(sin pi/4)
	
	glPushMatrix();{
		//glScalef(.5,1,1);
		glTranslatef(radius*cosf(3.1415926f/4),0,radius*sinf(3.1415926f/4));//shift the curve to center
		glRotatef(90,1,0,0);
		

		float cubesize_step = (top-base)/slices;
		for(int ii = 0; ii < slices; ii++) 
		{ 
			float theta = (3.1415926f*.75)+(2.0f * 3.1415926f * float(ii) / float(slices*4));//get the current angle 

			float x = radius * cosf(theta);//calculate the x component 
			float y = radius * sinf(theta);//calculate the y component 

			//glVertex2f(x + cx, y + cy);//output vertex 
		
			glPushMatrix();{
		
				glTranslatef(x,y,0);
				//glScalef(1,1,0.5);
				glutSolidCube(top);
		
			}glPopMatrix();
			top = top - cubesize_step;

		} 
	}glPopMatrix();

	
}

void drawRectangleCover(float width, float length, float slope_length,float slope_height) {
	float outerX = width/2;
	float outerY = length/2;

	float innerX = outerX-slope_length;
	float innerY = outerY-slope_length;

		glPushMatrix();{
		//glRotatef(70,1,0,0);

			//right
			glBegin(GL_QUADS);{
				glVertex3f(outerX,-outerY,0);
				glVertex3f(outerX,outerY,0);
				glVertex3f(innerX,innerY,slope_height);
				glVertex3f(innerX,-innerY,slope_height);
			}glEnd();
			//top
			glBegin(GL_QUADS);{
				glVertex3f(innerX,innerY,slope_height);
				glVertex3f(outerX,outerY,0);
				glVertex3f(-outerX,outerY,0);
				glVertex3f(-innerX,innerY,slope_height);
			}glEnd();
			//left
			glBegin(GL_QUADS);{
				glVertex3f(-innerX,innerY,slope_height);
				glVertex3f(-outerX,outerY,0);
				glVertex3f(-outerX,-outerY,0);
				glVertex3f(-innerX,-innerY,slope_height);
			}glEnd();
			//bottom
			glBegin(GL_QUADS);{
				glVertex3f(outerX,-outerY,0);
				glVertex3f(innerX,-innerY,slope_height);
				glVertex3f(-innerX,-innerY,slope_height);
				glVertex3f(-outerX,-outerY,0);
			}glEnd();
			
			//top cover
			glColor3ub(170,165,120);
			glBegin(GL_QUADS);{
				glVertex3f(innerX,-innerY,slope_height);
				glVertex3f(innerX,innerY,slope_height);
				glVertex3f(-innerX,innerY,slope_height);
				glVertex3f(-innerX,-innerY,slope_height);
			}glEnd();
				
		}glPopMatrix();


}

void drawRectangleCoverMirror(float width, float length, float slope_length,float slope_height) {
	float outerX = width/2;
	float outerY = length/2;

	float innerX = outerX-slope_length;
	float innerY = outerY-slope_length;

		glPushMatrix();{
		//glRotatef(70,1,0,0);

			//right
			glBegin(GL_QUADS);{
				glVertex3f(outerX,-outerY,0);
				glVertex3f(outerX,outerY,0);
				glVertex3f(innerX,innerY,slope_height);
				glVertex3f(innerX,-innerY,slope_height);
			}glEnd();
			//top
			glBegin(GL_QUADS);{
				glVertex3f(innerX,innerY,slope_height);
				glVertex3f(outerX,outerY,0);
				glVertex3f(-outerX,outerY,0);
				glVertex3f(-innerX,innerY,slope_height);
			}glEnd();
			//left
			glBegin(GL_QUADS);{
				glVertex3f(-innerX,innerY,slope_height);
				glVertex3f(-outerX,outerY,0);
				glVertex3f(-outerX,-outerY,0);
				glVertex3f(-innerX,-innerY,slope_height);
			}glEnd();
			//bottom
			glBegin(GL_QUADS);{
				glVertex3f(outerX,-outerY,0);
				glVertex3f(innerX,-innerY,slope_height);
				glVertex3f(-innerX,-innerY,slope_height);
				glVertex3f(-outerX,-outerY,0);
			}glEnd();
			
			//top cover
			//glColor3ub(170,165,120);
			glBegin(GL_QUADS);{
				glVertex3f(innerX,-innerY,slope_height);
				glVertex3f(innerX,innerY,slope_height);
				glVertex3f(-innerX,innerY,slope_height);
				glVertex3f(-innerX,-innerY,slope_height);
			}glEnd();
				
		}glPopMatrix();


}

void drawRectangle(float length,float width,float height) {
	glPushMatrix();{
		glScalef(length/width,1,height/width);
		glutSolidCube(width);
	}glPopMatrix();
}

void drawCenterObject() {
	float length = 70;
	float width = 40;
	float height = 5;

	//float legRadius = 10;
	//float legHeight = 2*legRadius*sinf(3.1415926f/4);//2lsin(pi/4)
	float legHeight = 15;
	float legBase = 1;
	float legTop = 4;

	float legX = width/2-legTop/2-1;
	float legY = length/2-legTop/2-1;

	
	//draw top cube
	glPushMatrix();{
		
		glColor3ub(100,85,70);
		glTranslatef(0,0,(height/2)+legHeight);
		drawRectangle(width,length,height);
		
		//top cover
		glColor3ub(120,90,50);
		glTranslatef(0,0,(height/2));
		drawRectangleCover(width,length,6,10);
		
	}glPopMatrix();

	
	
	//draw legs
	glPushMatrix();{
		
		glTranslatef(legX,-legY,0);
		//glRotatef(-90,0,0,1);
		glColor3ub(70,55,35);
		drawCurvedLegs(legHeight,legBase,legTop,40);
		
	}glPopMatrix();

	glPushMatrix();{
		
		glTranslatef(-legX,-legY,0);
		glRotatef(180,0,0,1);
		glColor3ub(70,55,35);
		drawCurvedLegs(legHeight,legBase,legTop,20);
		
	}glPopMatrix();

	glPushMatrix();{
		
		glTranslatef(legX,legY,0);
		//glRotatef(90,0,0,1);
		glColor3ub(70,55,35);
		drawCurvedLegs(legHeight,legBase,legTop,20);
		
	}glPopMatrix();

	glPushMatrix();{
		
		glTranslatef(-legX,legY,0);
		glRotatef(180,0,0,1);
		glColor3ub(70,55,35);
		drawCurvedLegs(legHeight,legBase,legTop,20);
		
	}glPopMatrix();

}

void drawCushion() {

	float width = 15;
	float curve_width = width - width/3;
	float depth = 2;

	float X = width/2;
	
	float dX = curve_width/2;
	
	glPushMatrix();{
		glTranslatef(0,0,X);
		glRotatef(70,1,0,0);

		glColor3ub(70, 130, 135);
		//front
		glBegin(GL_POLYGON);{
			
			glVertex3f(dX,0,depth);
			glVertex3f(X,X,0);
			glVertex3f(0,dX,depth);
			glVertex3f(-X,X,0);
			
			glVertex3f(-dX,0,depth);
			glVertex3f(-X,-X,0);
			glVertex3f(0,-dX,depth);
			glVertex3f(X,-X,0);
				
		}glEnd();
		//back
		glBegin(GL_POLYGON);{
			
			glVertex3f(dX,0,-depth);
			glVertex3f(X,X,0);
			glVertex3f(0,dX,-depth);
			glVertex3f(-X,X,0);
			
			glVertex3f(-dX,0,-depth);
			glVertex3f(-X,-X,0);
			glVertex3f(0,-dX,-depth);
			glVertex3f(X,-X,0);
				
		}glEnd();
		glColor3ub(75, 170, 160);
		//right
		glBegin(GL_POLYGON);{
			
			glVertex3f(dX,0,-depth);
			glVertex3f(X,X,0);
			glVertex3f(dX,0,depth);
			glVertex3f(X,-X,0);
				
		}glEnd();
		
		//top
		glBegin(GL_POLYGON);{
			
			glVertex3f(0,dX,-depth);
			glVertex3f(X,X,0);
			glVertex3f(0,dX,depth);
			glVertex3f(-X,X,0);
			
		}glEnd();
		
		//left
		glBegin(GL_POLYGON);{
			
			glVertex3f(-dX,0,-depth);
			glVertex3f(-X,X,0);
			glVertex3f(-dX,0,depth);
			glVertex3f(-X,-X,0);
			
		}glEnd();
		
		//bottom
		//left
		glBegin(GL_POLYGON);{
			glVertex3f(0,-dX,-depth);
			glVertex3f(-X,-X,0);
			glVertex3f(0,-dX,-depth);
			glVertex3f(X,-X,0);
				
		}glEnd();
				
		}glPopMatrix();

}

void drawSofa() {

	float sofa_width = 30;
	float sofa_length = 25;
	float handle_width = 6;
	float handle_height = 7;//only rectangle, not the cylinder

	float legHeight = 10;
	float bottom_foam_Height = 8;
	float top_foam_Height = 6;

	//bottom foam
	glPushMatrix();{
		glColor3ub(220,220,220);
		
		glTranslatef(0,0,(bottom_foam_Height/2)+legHeight);
		drawRectangle(sofa_width,sofa_length,bottom_foam_Height);
				
	}glPopMatrix();
	
	//top foam
	glPushMatrix();{
		float extention = 2;	
		
		glColor3ub(90,80,70);
		glTranslatef(0,-(extention/2),(top_foam_Height/2)+legHeight+bottom_foam_Height);
		drawRectangle(sofa_width,sofa_length+extention,top_foam_Height);
				
	}glPopMatrix();

	//handle rectangle
	glColor3ub(122,118,114);
	glPushMatrix();{
		//glColor3ub(217,209,220);
		
		glTranslatef((sofa_width-handle_width)/2,0,legHeight+bottom_foam_Height+top_foam_Height+(handle_height/2));
		drawRectangle(handle_width,sofa_length,handle_height);
		
		glTranslatef(-(sofa_width-handle_width),0,0);
		drawRectangle(handle_width,sofa_length,handle_height);
		
				
	}glPopMatrix();
	
	//handle cylinder
	glPushMatrix();{
		GLUquadricObj *quadObj = gluNewQuadric();

		glTranslatef((sofa_width-handle_width)/2,(sofa_length/2),(legHeight+bottom_foam_Height+top_foam_Height+handle_height));
		glRotatef(90,1,0,0);

		gluCylinder(quadObj, handle_width/2, handle_width/2, sofa_length, 40, 40); //gluCylinder(quadObj, base, top, height, slices, stacks)
		
		glPushMatrix();{
			glTranslatef(0,0,sofa_length);
			gluDisk(quadObj, 0,handle_width/2 , 40, 40); //gluDisk(quadObj, innerRadius, outerRadius, slices, loops)
		}glPopMatrix();

		glTranslatef(-(sofa_width-handle_width),0,0);
		gluCylinder(quadObj, handle_width/2, handle_width/2, sofa_length, 40, 40); //gluCylinder(quadObj, base, top, height, slices, stacks)
		
		glPushMatrix();{
			glTranslatef(0,0,sofa_length);
			gluDisk(quadObj, 0,handle_width/2 , 40, 40); //gluDisk(quadObj, innerRadius, outerRadius, slices, loops)
		}glPopMatrix();
		
	}glPopMatrix();

	//sofa back rectangular
	glColor3ub(195,195,195);
	float sofa_back_height = bottom_foam_Height+top_foam_Height+handle_height+10;//extra 20 height at back
	
	glPushMatrix();{
		//glColor3ub(185,170,155);
		glTranslatef(0,sofa_length/2,(sofa_back_height/2)+legHeight);
		drawRectangle(sofa_width+0.2,handle_width,sofa_back_height);
				
	}glPopMatrix();
	
	//sofa back cylinder
	glPushMatrix();{
		GLUquadricObj *quadObj = gluNewQuadric();
		//glColor3ub(185,170,155);
		
		glTranslatef(-(sofa_width)/2,(sofa_length/2),sofa_back_height+legHeight);
		glRotatef(90,0,1,0);
		gluCylinder(quadObj, handle_width/2, handle_width/2, sofa_width+0.2, 40, 40); //gluCylinder(quadObj, base, top, height, slices, stacks)
		
		glPushMatrix();{
			gluDisk(quadObj, 0,handle_width/2 , 40, 40); //gluDisk(quadObj, innerRadius, outerRadius, slices, loops)
			glTranslatef(0,0,sofa_width);
			gluDisk(quadObj, 0,handle_width/2 , 40, 40); //gluDisk(quadObj, innerRadius, outerRadius, slices, loops)
		}glPopMatrix();

	}glPopMatrix();
	
	//sofa 4 legs
	float legX = sofa_width/2-2;
	float legY = sofa_length/2-2;
	
	glPushMatrix();{
		GLUquadricObj *quadObj = gluNewQuadric();
		glColor3ub(135,80,20);
		glTranslatef(legX,-legY,0);
		gluCylinder(quadObj, 0.5, 2, legHeight, 30, 30);

	}glPopMatrix();
	glPushMatrix();{
		GLUquadricObj *quadObj = gluNewQuadric();
		glColor3ub(135,80,20);
		glTranslatef(-legX,-legY,0);
		gluCylinder(quadObj,  0.5, 2, legHeight, 30, 30);

	}glPopMatrix();

	glPushMatrix();{
		GLUquadricObj *quadObj = gluNewQuadric();
		glColor3ub(135,80,20);
		glTranslatef(legX,legY,0);
		gluCylinder(quadObj, 0.5, 2, legHeight, 30, 30);

	}glPopMatrix();
	glPushMatrix();{
		GLUquadricObj *quadObj = gluNewQuadric();
		glColor3ub(135,80,20);
		glTranslatef(-legX,legY,0);
		gluCylinder(quadObj,  0.5, 2, legHeight, 30, 30);

	}glPopMatrix();

	glPushMatrix();{
		
		glTranslatef(0,(sofa_length/2)-handle_width-3,legHeight+bottom_foam_Height+top_foam_Height);
		drawCushion();

	}glPopMatrix();

}

void drawChairFrontLeg(float width, float height) {
	

	glPushMatrix();{
		GLUquadricObj *quadObj = gluNewQuadric();
		//gluCylinder(quadObj, width/6, width/2, bottom_height, 40, 40);//bottom cone cylinder
		glPushMatrix();{
			glRotatef(-90,0,0,1);
			glScalef(1,2,1);
			drawCurvedLegs(height-2,width/3, width/2,40);
		}glPopMatrix();
		//glTranslatef(0,0,bottom_height);
		//gluCylinder(quadObj, width/2, width/2, height-bottom_height, 40, 40);

	}glPopMatrix();
}

void drawRoundSheet(float radius,float height_curve) {

	//float radius_circle = ((radius*radius)+(height_curve*height_curve))/(2*height_curve); //r = (x^2+h^2)/2h ; x = given sheet radius

	float cut_point = radius-height_curve; //draw only if z>=cut_point
	
	glPushMatrix();{
		glTranslatef(0,0,height_curve-radius);//push down to r-h
		
		double equ[4];

		equ[0] = 0;	//0.x
		equ[1] = 0;	//0.y
		equ[2] = 1;//-1.z
		equ[3] = -cut_point;//30

		glClipPlane(GL_CLIP_PLANE0,equ);
		glEnable(GL_CLIP_PLANE0);{
			glPushMatrix();{
				
				glutSolidSphere(radius,40,40);
				
			}glPopMatrix();
		}glDisable(GL_CLIP_PLANE0);
	}glPopMatrix();
}

void drawChair(){
	float Cwidth = 30;
	float Clength = 35;

	float Cbackwidth = 2*Cwidth/3;

	float seatheight = 10;
	float legheight = 25;
	float legwidth = 3;
	
	float handleheight = 15;
	float handle_backward = 7;

	float back_seat_height = 30;
	float back_seat_rotation = 5;

	//chair seat
	glPushMatrix();{
		
		glTranslatef(0,0,legheight);

		//seat er uporer foam
		glPushMatrix();{
			glColor3ub(135,145,140);
			glTranslatef(0,Cwidth/2,seatheight);

			drawRoundSheet(Cwidth,4);
		}glPopMatrix();
		
		double equ[4];
		//draw y>=0
		equ[0] = 0;	//0.x
		equ[1] = 1;	//0.y
		equ[2] = 0;//-1.z
		equ[3] = 0;//0

		glClipPlane(GL_CLIP_PLANE0,equ);
		glEnable(GL_CLIP_PLANE0);{
			glPushMatrix();{
				GLUquadricObj *quadObj = gluNewQuadric();
				glColor3ub(65,100,80);
		
				glScalef(1,2*(Clength/Cwidth),1);
				gluCylinder(quadObj, Cwidth/2, Cwidth/2, seatheight, 40, 40); //gluCylinder(quadObj, base, top, height, slices, stacks)
		
				glPushMatrix();{
					gluDisk(quadObj, 0,Cwidth/2 , 40, 40); //gluDisk(quadObj, innerRadius, outerRadius, slices, loops)
					glTranslatef(0,0,seatheight);
					gluDisk(quadObj, 0,Cwidth/2 , 40, 40); //gluDisk(quadObj, innerRadius, outerRadius, slices, loops)
				}glPopMatrix();

			}glPopMatrix();
		}glDisable(GL_CLIP_PLANE0);
	}glPopMatrix();

	//chair handle
	glPushMatrix();{
		glTranslatef(0,handle_backward,legheight+seatheight+handleheight);
		
			glPushMatrix();{
		
			double equ[4];
			//draw y>=0
			equ[0] = 0;	//0.x
			equ[1] = 1;	//0.y
			equ[2] = 0;//-1.z
			equ[3] = 0;//0

			glClipPlane(GL_CLIP_PLANE0,equ);
			glEnable(GL_CLIP_PLANE0);{
				glPushMatrix();{
					GLUquadricObj *quadObj = gluNewQuadric();
					glColor3ub(70,55,35);
		
					glScalef(1,2,1);
					glutSolidTorus(legwidth/2,(Cwidth-legwidth)/2,40,40);
		
					/*glPushMatrix();{
						gluDisk(quadObj, 0,Cwidth/2 , 40, 40); //gluDisk(quadObj, innerRadius, outerRadius, slices, loops)
						glTranslatef(0,0,seatheight);
						gluDisk(quadObj, 0,Cwidth/2 , 40, 40); //gluDisk(quadObj, innerRadius, outerRadius, slices, loops)
					}glPopMatrix();
					*/
				}glPopMatrix();
			}glDisable(GL_CLIP_PLANE0);
		}glPopMatrix();

	}glPopMatrix();
	
	//chair handle vertical cylinder
	glPushMatrix();{
		GLUquadricObj *quadObj = gluNewQuadric();
		glColor3ub(70,55,35);
		
		glTranslatef((Cwidth-legwidth)/2,handle_backward,legheight+seatheight);
		gluCylinder(quadObj, legwidth/2, legwidth/2, handleheight, 40, 40); //gluCylinder(quadObj, base, top, height, slices, stacks)
		
		glTranslatef(-(Cwidth-legwidth),0,0);
		gluCylinder(quadObj, legwidth/2, legwidth/2, handleheight, 40, 40); //gluCylinder(quadObj, base, top, height, slices, stacks)
		
	}glPopMatrix();
	
	
	//2 cylinder of back seat
	glPushMatrix();{
		
		GLUquadricObj *quadObj = gluNewQuadric();
		
		//right one
		glTranslatef(Cbackwidth/2,Clength-2,legheight);
		glPushMatrix();{
			glRotatef(-back_seat_rotation,1,0,0);
			gluCylinder(quadObj, legwidth/2, legwidth/2, back_seat_height, 40, 40);
		}glPopMatrix();
		
		//left one
		glTranslatef(-Cbackwidth,0,0);
		glPushMatrix();{
			glRotatef(-back_seat_rotation,1,0,0);
			
			gluCylinder(quadObj, legwidth/2, legwidth/2, back_seat_height, 40, 40);
		}glPopMatrix();
		
	}glPopMatrix();
	
	//chair back foam
	glPushMatrix();{
		glColor3ub(135,145,140);
		glTranslatef(0,Clength,legheight+back_seat_height);
		glRotatef(90-back_seat_rotation,1,0,0);

		drawRoundSheet(Cbackwidth-legwidth,4);
	}glPopMatrix();
	
	//chair back torus(on top of back)
	glPushMatrix();{
		glTranslatef(0,Clength+0.5,legheight+back_seat_height);
		glRotatef(90-back_seat_rotation,1,0,0);
		
		glPushMatrix();{
		
			double equ[4];
			//draw y>=0
			equ[0] = 0;	//0.x
			equ[1] = 1;	//0.y
			equ[2] = 0;//-1.z
			equ[3] = 0;//0

			glClipPlane(GL_CLIP_PLANE0,equ);
			glEnable(GL_CLIP_PLANE0);{
				glPushMatrix();{

					GLUquadricObj *quadObj = gluNewQuadric();
					glColor3ub(70,55,35);
		
					//glScalef(1,5,1);
					
					glutSolidTorus(legwidth/2,Cbackwidth/2,40,40);
		
					/*glPushMatrix();{
						gluDisk(quadObj, 0,Cwidth/2 , 40, 40); //gluDisk(quadObj, innerRadius, outerRadius, slices, loops)
						glTranslatef(0,0,seatheight);
						gluDisk(quadObj, 0,Cwidth/2 , 40, 40); //gluDisk(quadObj, innerRadius, outerRadius, slices, loops)
					}glPopMatrix();
					*/
				}glPopMatrix();
			}glDisable(GL_CLIP_PLANE0);
		}glPopMatrix();

	}glPopMatrix();
	
	//draw chair front legs
	glPushMatrix();{
		glColor3ub(70,55,35);
		
		glTranslatef((Cwidth-legwidth)/2,legwidth/2,0); //front right leg
		glPushMatrix();{
			glRotatef(-5,1,0,0);
			drawChairFrontLeg(legwidth,legheight);
		}glPopMatrix();
		
		glTranslatef(-(Cwidth-legwidth),legwidth/2,0); //front left leg
		glPushMatrix();{
			glRotatef(-5,1,0,0);
			drawChairFrontLeg(legwidth,legheight);
		}glPopMatrix();
			
	}glPopMatrix();

	//draw chair back legs
	glPushMatrix();{
		
		glColor3ub(70,55,35);
		glTranslatef((Cbackwidth-legwidth)/2,Clength-(legwidth/2),0);
		
		glPushMatrix();{
			glRotatef(90,0,0,1);
			drawCurvedLegs(legheight,legwidth,legwidth,40);
		}glPopMatrix();
		glTranslatef(-(Cbackwidth-legwidth),0,0);
		
		glPushMatrix();{
			glRotatef(90,0,0,1);
			drawCurvedLegs(legheight,legwidth,legwidth,40);
		}glPopMatrix();
	}glPopMatrix();

	
}

void drawRectangleMirror() {
	float width = 25;
	float height = 35;
	float depth = 1;
	
	glPushMatrix();{
		glRotatef(90,1,0,0);
		glPushMatrix();{
			glColor3ub(35,65,75);
			drawRectangle(width,height,depth);
			glTranslatef(0,0,depth);
		
			glColor3ub(217,230,238);
			drawRectangleCoverMirror(width-2,height-2,1.5,1);
		}glPopMatrix();
	}glPopMatrix();
}

void drawMirror(float outer_radius,float inner_radius,float depth) {
	glPushMatrix();{
		
		GLUquadricObj *quadObj = gluNewQuadric();
	
		glColor3ub(35,65,75);
		gluCylinder(quadObj, outer_radius, inner_radius, depth, 40, 10); //gluCylinder(quadObj, base, top, height, slices, stacks)
	
		glColor3ub(217,230,238);
		glTranslatef(0,0,depth);//30 top er height
		gluDisk(quadObj, 0, inner_radius, 40, 40); //gluDisk(quadObj, innerRadius, outerRadius, slices, loops)
		

	}glPopMatrix();
}

void drawLampTable() {
	float radius = 20;
	float legheight = 45;

	float leg_inside = 10;
	float legwidth = 2;

	float legX = radius-leg_inside;
	float legY = radius-leg_inside;
	glPushMatrix();{
		
		GLUquadricObj *quadObj = gluNewQuadric();
		glColor3ub(35,65,75);

		glTranslatef(0,0,legheight+1);
		gluCylinder(quadObj, radius, radius, 2, 30, 30); //gluCylinder(quadObj, base, top, height, slices, stacks)
		gluDisk(quadObj,0,radius,30,30);
		glTranslatef(0,0,2);
	}glPopMatrix();
		//draw legs
	glPushMatrix();{
		
		glTranslatef(legX,-legY,0);
		//glRotatef(-90,0,0,1);
		glColor3ub(70,55,35);
		drawCurvedLegs(legheight,legwidth,legwidth,40);
		
	}glPopMatrix();

	glPushMatrix();{
		
		glTranslatef(-legX,-legY,0);
		glRotatef(180,0,0,1);
		glColor3ub(70,55,35);
		drawCurvedLegs(legheight,legwidth,legwidth,40);
		
	}glPopMatrix();

	glPushMatrix();{
		
		glTranslatef(legX,legY,0);
		//glRotatef(90,0,0,1);
		glColor3ub(70,55,35);
		drawCurvedLegs(legheight,legwidth,legwidth,40);
		
	}glPopMatrix();

	glPushMatrix();{
		
		glTranslatef(-legX,legY,0);
		glRotatef(180,0,0,1);
		glColor3ub(70,55,35);
		drawCurvedLegs(legheight,legwidth,legwidth,40);
		
	}glPopMatrix();

	
}

void drawRoundMirror() {
	float outer_radius = 10;
	float inner_radius = 9;
	float depth = 1;

	float atom_inner = outer_radius+2;
	float atom_outer = 30;
	float atom_slices = 30;
	
	glPushMatrix();{
		glRotatef(90,1,0,0);
		//center ma mirror
		drawMirror(outer_radius,inner_radius,depth);

		//charpashe baccha mirror
		int slices = 30;
		for(int ii = 0; ii < slices; ii++) 
			{ 
				float theta = (2.0f * 3.1415926f * float(ii) / float(slices));//get the current angle 

				float atom_radius = 1;
				for(int r = atom_inner; r<atom_outer; r+=4) {
					float x = r * cosf(theta);//calculate the x component 
					float y = r * sinf(theta);//calculate the y component 
		
					glPushMatrix();{
		
						glTranslatef(x,y,0);
						//glScalef(1,1,0.5);
						//glutSolidCube(2);
						drawMirror(atom_radius,atom_radius-1,1);
						atom_radius += 0.4;
		
					}glPopMatrix();
				
				}

			}
	}glPopMatrix();
}

void drawFirePlace() {
	float width = 40;
	float height = 50;

	float pillar_width = 10;
	float pillar_depth = 5;

	float top_extension = 2;
	float top_extension_height = 2;

	//two pillars on right and left
	glPushMatrix();{
		glColor3ub(190,190,190);
		glTranslatef((width+pillar_width)/2,0,height/2);//right one
		drawRectangle(pillar_width,pillar_depth,height);

		glTranslatef(-(width+pillar_width),0,0);//left one
		drawRectangle(pillar_width,pillar_depth,height);
	}glPopMatrix();

	//top horizontal pillar
	glPushMatrix();{
		glColor3ub(190,190,190);
		glTranslatef(0,0,height+(pillar_width/2));
		drawRectangle(width+(2*pillar_width),pillar_depth,pillar_width);

		//top extension
		glColor3ub(130,140,145);
		glTranslatef(0,-(top_extension),(pillar_width+top_extension_height)/2);
		drawRectangle(width+(2*pillar_width)+top_extension,pillar_depth+(2*top_extension),top_extension_height);

	}glPopMatrix();

	glPushMatrix();{
		glColor3ub(0,0,0);
		glTranslatef(0,0,height/2);
		glRotatef(-90,1,0,0);
		drawRectangle(width+2,height+2,3);

	}glPopMatrix();
}

void drawCurtain(int curtain) {
	float width = 100;
	float curtain_width = width/max_curtain;
	float curtain_height = 110;
	float wall_height = 150;

	glColor3ub(30,110,70);

	//curtain er uporer fixed curtain ta
	glPushMatrix();{
		
		glTranslatef(0,0,curtain_height+((wall_height-curtain_height)/2));
		drawRectangle(2*width-curtain_width,2,(wall_height-curtain_height));
	}glPopMatrix();
	
	for(int i = 1;i<=curtain;i++) {
		glPushMatrix();{
		
			glTranslatef(width-(curtain_width*i),0,curtain_height/2);
			drawRectangle(curtain_width-0.5,2,curtain_height);
		}glPopMatrix();
		

		glPushMatrix();{
		
		glTranslatef(-(width-(curtain_width*i)),0,curtain_height/2);
		drawRectangle(curtain_width-0.5,2,curtain_height);
		}glPopMatrix();


	}
}


void drawWall() {
	float width = 200;
	float height = 150;
	float depth = 5;

	float X = (width+depth)/2;
	float Y = (width+depth)/2;

	glColor3ub(166,190,200);
	

	//right
	
	glPushMatrix();{
		glTranslatef(X,0,height/2);
		glPushMatrix();{
			glRotatef(90,0,0,1);
			drawRectangle(width-0.5,depth,height);
		}glPopMatrix();

	}glPopMatrix();
	
	
	//left
	glPushMatrix();{
		glTranslatef(-X,0,height/2);
		glPushMatrix();{
			glRotatef(90,0,0,1);
			drawRectangle(width-0.5,depth,height);
			
		}glPopMatrix();

	}glPopMatrix();

	//top
	glPushMatrix();{
		glTranslatef(0,Y,height/2);
		drawRectangle(width-0.5,depth,height);
		
	}glPopMatrix();
	
	//bottom
	glPushMatrix();{
		glTranslatef(0,-Y,height/2);
		drawRectangle(width-0.5,depth,height);
		
	}glPopMatrix();
	
	

}

void decorateObject () {
	glPushMatrix();{
		glTranslatef(0,98,0);
		drawCurtain(curtain_count);
		
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(0,70,0);
		drawSofa();
		
		glTranslatef(-50,0,0);
		drawSofa();
		
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(-80,65,0);
		drawLampTable();
		
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(-80,65,50);
		drawLamp();
		
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(-30,0,0);
		drawCenterObject();
		
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(-95,0,0);
		glPushMatrix();{
		
			glRotatef(90,0,0,1);
			drawFirePlace();
		
		}glPopMatrix();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(-98,0,100);
		glPushMatrix();{
		
			glRotatef(90,0,0,1);
			drawRoundMirror();
		
		}glPopMatrix();
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(-98,50,90);
		glPushMatrix();{
		
			glRotatef(90,0,0,1);
			drawRectangleMirror();
		
		}glPopMatrix();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(-60,-60,0);
		glPushMatrix();{
		
			glRotatef(135,0,0,1);
			drawChair();
		}glPopMatrix();
	}glPopMatrix();
}



void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(BLACK, 0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
	/ set-up camera here
	********************/
	//load the correct matrix -- MODEL-VIEW matrix
	glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera is looking?
	//3. Which direction is the camera's UP direction?

	//gluLookAt(0,-100,80,	0,0,0,	0,0,1);//static camera
	if(center_camera == 1) {
		gluLookAt(0,0,cameraFromZ,	100*sin(cameraAngle),100*cos(cameraAngle),0,	0,0,1);
	}
	
	else if(rotate_camera ==1) {
		gluLookAt(100*sin(cameraAngle),100*cos(cameraAngle),cameraFromZ,  0,0,0,	0,0,1);
	}
	
	else {
		gluLookAt(cameraFromX,cameraFromY,cameraFromZ,	0,0,0,	0,0,1);
	}
	//gluLookAt(cameraFromX,cameraFromY,cameraFromZ,	0,0,0,	0,0,1);
	//gluLookAt(0,0,100,	100*sin(cameraAngle),150*cos(cameraAngle),0,	0,0,1);
	
	//again select MODEL-VIEW
	glMatrixMode(GL_MODELVIEW);


	/****************************
	/ Add your objects from here
	****************************/
	//add objects
	
	//test object
	glPushMatrix();{

		if(showWall == 1)
			drawWall();

		decorateObject();
		
		
	}glPopMatrix();

	//floor
	glPushMatrix();{
		glColor3ub(115,128,132);
		drawRectangle(200,200,1);
	}glPopMatrix();


	//some gridlines along the field
	int i;

	glColor3f(0.5, 0.5, 0.5);	//grey
	glBegin(GL_LINES);{
		for(i=-10;i<=10;i++){

			if(i==0)
				continue;	//SKIP the MAIN axes

			//lines parallel to Y-axis
			glVertex3f(i*10, -100, 0);
			glVertex3f(i*10,  100, 0);

			//lines parallel to X-axis
			glVertex3f(-100, i*10, 0);
			glVertex3f( 100, i*10, 0);
		}
	}glEnd();
	
	// draw the two AXES
	glColor3f(1, 1, 1);	//100% white
	glBegin(GL_LINES);{
		//Y axis
		glColor3f(0,1,0);
		glVertex3f(0, -150, 0);	// intentionally extended to -150 to 150, no big deal
		glVertex3f(0,  150, 0);

		//X axis
		glColor3f(1,0,0);
		glVertex3f(-100, 0, 0);
		glVertex3f( 100, 0, 0);
	}glEnd();


	//ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glutSwapBuffers();
}

void animate(){
	//codes for any changes in Models, Camera
	if(center_camera == 1||rotate_camera == 1){
		if(camera_pause != 1)
			cameraAngle += 0.005;
	}
	if(curtain_open==1 && curtain_count<max_curtain) {
		 
		Sleep(100);
		curtain_count++;
	}
	if(curtain_open==0 && curtain_count>min_curtain) {
		Sleep(200);
		curtain_count--;
	}
	glutPostRedisplay();
}


void keyboardListener(unsigned char key, int x,int y){
	switch(key){

		case '1':	//reverse the rotation of camera
			//cameraAngleDelta = -cameraAngleDelta;
			//cameraFromX+=10;
			curtain_open = 1;
			break;

		case '2':	//increase rotation of camera by 10%
			//cameraAngleDelta *= 1.1;
			curtain_open = 0;
			break;

		case '3':	//decrease rotation
			//cameraAngleDelta /= 1.1;
			break;

		case '8':	//toggle grids
			//canDrawGrid = 1 - canDrawGrid;
			break;

		case 'c':	

			rotate_camera = 0;// turn off rotate camera
			if(center_camera == 1) 
				center_camera = 0;
			else
				center_camera = 1;
			break;
		case 'w':	
			if(showWall == 1) 
				showWall = 0;
			else
				showWall = 1;
			break;
		case 'r':
			center_camera = 0;//turn off center camera
			if(rotate_camera == 1) 
				rotate_camera = 0;
			else
				rotate_camera = 1;
			break;
		case 'p':
			
			if(camera_pause == 1) 
				camera_pause = 0;
			else
				camera_pause = 1;
			break;

		case 27:	//ESCAPE KEY -- simply exit
			exit(0);
			break;

		default:
			break;
	}
}

void specialKeyListener(int key, int x,int y){
	switch(key){
		case GLUT_KEY_DOWN:		//down arrow key
			cameraFromY -= 10;
			break;
		case GLUT_KEY_UP:		// up arrow key
			cameraFromY += 10;
			break;

		case GLUT_KEY_RIGHT:
			cameraFromX += 10;
			break;
		case GLUT_KEY_LEFT:
			cameraFromX -= 10;
			break;

		case GLUT_KEY_PAGE_UP:
			cameraFromZ += 10;
			break;
		case GLUT_KEY_PAGE_DOWN:
			cameraFromZ -= 10;
			break;

		case GLUT_KEY_INSERT:
			break;

		case GLUT_KEY_HOME:
			break;
		case GLUT_KEY_END:
			break;

		default:
			break;
	}
}

void init(){
	//codes for initialization
	cameraAngle = 0;
	center_camera = 0;
	rotate_camera = 0;
	camera_pause = 0;

	showWall = 1;

	cameraFromX = 100;
	cameraFromY = -100;
	cameraFromZ = 200;

	curtain_count = min_curtain;
	//clear the screen
	glClearColor(BLACK, 0);

	/************************
	/ set-up projection here
	************************/
	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);
	
	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(70,	1,	0.1,	10000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
}

int main(int argc, char **argv){
	glutInit(&argc,argv);
	glutInitWindowSize(900, 600);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("My OpenGL Program");

	init();

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}


