#include "stdafx.h"


#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "time.h"
#include "windows.h"
#include "GL/glut.h"

#define CLR_BLACK 0, 0, 0
#define CLR_WHITE 190,190,190
#define CLR_WHITE1 255,255,255

#define CLR_BRICK 202,100,85
#define CLR_BRICK2 165,80,70
#define CLR_BRICK3 150,55,50

#define CLR_DARKER_GRAY 60,60,60
#define CLR_DARKER2_GRAY 40,40,40
#define CLR_DARK_GRAY 90,90,90
#define CLR_LIGHT_GRAY 120,120,120
#define CLR_LIGHTER_GRAY 140,140,140

#define CLR_GOLDEN 210,205,130


struct Color {
int r,g,b;
Color(int r_,int g_,int b_):r(r_),g(g_),b(b_) {}
};


double cameraFromX,cameraFromY,cameraFromZ;
double cameraToX,cameraToY,cameraToZ;


int rotateXY,rotateYZ,rotateZX;
double cameraAngle;
double lightAngle;

int center_camera;

int rotate_camera;

int camera_pause;

int spot;

int initial_pos;

int isDayMode, isWhiteLight,isLighting;

GLUquadricObj *quadObj = gluNewQuadric();


GLuint tex_white_wall,tex_redwall,tex_graywall,tex_pinkwall,tex_gray_bakano_rec,tex_janala_bg,tex_tomb_janala,tex_brick,tex_tupi;
int num_texture = -1;

int LoadBitmap(char *filename)
{
    int i, j=0;
    FILE *l_file;
    unsigned char *l_texture;

    BITMAPFILEHEADER fileheader;
    BITMAPINFOHEADER infoheader;
    RGBTRIPLE rgb;

    num_texture++;

    if( (l_file = fopen(filename, "rb"))==NULL) return (-1);

    fread(&fileheader, sizeof(fileheader), 1, l_file);

    fseek(l_file, sizeof(fileheader), SEEK_SET);
    fread(&infoheader, sizeof(infoheader), 1, l_file);

    l_texture = (byte *) malloc(infoheader.biWidth * infoheader.biHeight * 4);
    memset(l_texture, 0, infoheader.biWidth * infoheader.biHeight * 4);

 for (i=0; i < infoheader.biWidth*infoheader.biHeight; i++)
    {
            fread(&rgb, sizeof(rgb), 1, l_file);

            l_texture[j+0] = rgb.rgbtRed;
            l_texture[j+1] = rgb.rgbtGreen;
            l_texture[j+2] = rgb.rgbtBlue;
            l_texture[j+3] = 255;
            j += 4;
    }
    fclose(l_file);

    glBindTexture(GL_TEXTURE_2D, num_texture);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, infoheader.biWidth, infoheader.biHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, l_texture);
     gluBuild2DMipmaps(GL_TEXTURE_2D, 4, infoheader.biWidth, infoheader.biHeight, GL_RGBA, GL_UNSIGNED_BYTE, l_texture);

    free(l_texture);

    return (num_texture);

}

void initTexture(void)
{

	tex_white_wall =LoadBitmap("white_wall.bmp");
	tex_redwall =LoadBitmap("red_wall.bmp");
	tex_graywall =LoadBitmap("gray_wall.bmp");
	tex_pinkwall =LoadBitmap("pink_wall.bmp");
	tex_gray_bakano_rec =LoadBitmap("gray_bakano_rec.bmp");
	tex_janala_bg =LoadBitmap("janala_bg.bmp");
	tex_tomb_janala =LoadBitmap("tomb_janala.bmp");
	tex_brick =LoadBitmap("brick.bmp");
	tex_tupi =LoadBitmap("tupi.bmp");

}

class Vector {
public:
	double x, y, z;
	Vector() {
	
	}

	Vector getOppositeVector(){
		return Vector(-x, -y, -z);
	}
	Vector scale(double f){
		return Vector(x * f, y * f, z * f);
	}
	Vector translate(Vector v){
		return Vector(x + v.x, y + v.y, z + v.z);
	}

	Vector getUnitVector(){
		double r = sqrt(x*x + y*y + z*z);
		return Vector(x / r, y / r, z / r);
	}

	Vector(double x, double y, double z){
		this->x = x;
		this->y = y;
		this->z = z;
	}
	
};

class Camera {
	float matrix[16];
	Vector Xaxis,Yaxis,Zaxis;

	void init(){
		glGetFloatv(GL_MODELVIEW_MATRIX, matrix);
		Xaxis = Vector(matrix[0], matrix[4], matrix[8]).getUnitVector();
		Yaxis = Vector(matrix[2], matrix[6], matrix[10]).getUnitVector();
		Zaxis = Vector(matrix[1], matrix[5], matrix[9]).getUnitVector();
	}

public:
	Vector position,lookat,updirection;

	void reset(){
		if(initial_pos == 0) {
			position = Vector(-15, 90, 45);
			lookat = Vector(0, 0, 0);
			updirection = Vector(0, 0, 1);
		}
		if(initial_pos == 1) {
			position = Vector(90, 0, 45);
			lookat = Vector(0, 0, 0);
			updirection = Vector(0, 0, 1);
		}

		if(initial_pos == 2) {
			position = Vector(15, -90, 45);
			lookat = Vector(0, 0, 0);
			updirection = Vector(0, 0, 1);
		}

		if(initial_pos == 3) {
			position = Vector(-90, 0, 45);
			lookat = Vector(0, 0, 0);
			updirection = Vector(0, 0, 1);
		}

	}



	void Yaw(double angle){
		glPushMatrix(); {
			glRotatef(angle, Yaxis.x, Yaxis.y, Yaxis.z);
            init();
        } glPopMatrix();
		lookat = position.translate(Yaxis.getOppositeVector());
		updirection = Zaxis;
	}
	void Pitch(double angle){
		glPushMatrix(); {
            glRotatef(angle, Xaxis.x, Xaxis.y, Xaxis.z);
            init();
        } glPopMatrix();
        lookat = position.translate(Yaxis.getOppositeVector());
		updirection = Zaxis;
	}

	void Roll(double angle){
		glPushMatrix(); {
			glRotatef(angle, Zaxis.x, Zaxis.y, Zaxis.z);
            init();
        } glPopMatrix();
		lookat = position.translate(Yaxis.getOppositeVector());
		updirection = Zaxis;
	}

	void walk(double change){
		init();
		Vector d = Yaxis.scale(change);
		position = position.translate(d);
		lookat = lookat.translate(d);
	}

	void straf(double change){
		init();
		Vector d = Xaxis.scale(change);
		position = position.translate(d);
		lookat = lookat.translate(d);
	}

	void fly(double change){
		init();
		Vector d = Zaxis.scale(change);
		position = position.translate(d);
		lookat = lookat.translate(d);
	}
	Camera(){
		reset();
	}
}camera;


void drawRectangle(float length,float width,float height) {
	glPushMatrix();{
		glTranslatef(0,0,height/2);
		glScalef(length/width,1,height/width);
		glutSolidCube(width);
	}glPopMatrix();
}

/*void drawRectangle(float length,float width,float height) {
	float X = length/2;
	float Y = width/2;
	float Z = height;
	
	//right
	glBegin(GL_POLYGON);{
			
		glVertex3f(X,-Y,0);
		glVertex3f(X,Y,0);
		glVertex3f(X,Y,Z);
		glVertex3f(X,-Y,Z);
				
	}glEnd();
	//left
	glBegin(GL_POLYGON);{
			
		glVertex3f(-X,-Y,0);
		glVertex3f(-X,Y,0);
		glVertex3f(-X,Y,Z);
		glVertex3f(-X,-Y,Z);
				
	}glEnd();
	//back
	glBegin(GL_POLYGON);{
			
		glVertex3f(-X,Y,0);
		glVertex3f(X,Y,0);
		glVertex3f(X,Y,Z);
		glVertex3f(-X,Y,Z);
				
	}glEnd();
	//front
	glBegin(GL_POLYGON);{
			
		glVertex3f(-X,-Y,0);
		glVertex3f(X,-Y,0);
		glVertex3f(X,-Y,Z);
		glVertex3f(-X,-Y,Z);
				
	}glEnd();
	//top
	
	
	glBegin(GL_POLYGON);{
			
		glVertex3f(-X,-Y,Z);
		glVertex3f(X,-Y,Z);
		glVertex3f(X,Y,Z);
		glVertex3f(-X,Y,Z);
				
	}glEnd();
	
}*/

void drawRectangleWithTexture(float length,float width,float height, GLuint texid) {
	float X = length/2;
	float Y = width/2;
	float Z = height;
	
	//right
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		glTexCoord2f(0,0);glVertex3f(X,-Y,0);
		glTexCoord2f(1,0);glVertex3f(X,Y,0);
		glTexCoord2f(1,1);glVertex3f(X,Y,Z);
		glTexCoord2f(0,1);glVertex3f(X,-Y,Z);
				
	}glEnd();
	glDisable(GL_TEXTURE_2D);
	//left
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		glTexCoord2f(0,0);glVertex3f(-X,-Y,0);
		glTexCoord2f(1,0);glVertex3f(-X,Y,0);
		glTexCoord2f(1,1);glVertex3f(-X,Y,Z);
		glTexCoord2f(0,1);glVertex3f(-X,-Y,Z);
				
	}glEnd();
	glDisable(GL_TEXTURE_2D);
	//back
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		glTexCoord2f(0,0);glVertex3f(-X,Y,0);
		glTexCoord2f(1,0);glVertex3f(X,Y,0);
		glTexCoord2f(1,1);glVertex3f(X,Y,Z);
		glTexCoord2f(0,1);glVertex3f(-X,Y,Z);
				
	}glEnd();
	glDisable(GL_TEXTURE_2D);
	//front
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		glTexCoord2f(0,0);glVertex3f(-X,-Y,0);
		glTexCoord2f(1,0);glVertex3f(X,-Y,0);
		glTexCoord2f(1,1);glVertex3f(X,-Y,Z);
		glTexCoord2f(0,1);glVertex3f(-X,-Y,Z);
				
	}glEnd();
	glDisable(GL_TEXTURE_2D);
	//top
	
	
	glBegin(GL_POLYGON);{
			
		glVertex3f(-X,-Y,Z);
		glVertex3f(X,-Y,Z);
		glVertex3f(X,Y,Z);
		glVertex3f(-X,Y,Z);
				
	}glEnd();
	
}

void minar_top()
{
    float height = 20;
	float in_radius = 0.75;
	double equ[4];

	equ[0] = -3;	//0.x
	equ[1] = 0;	//0.y
	equ[2] = -1;//-1.z
	equ[3] = 25;//30

	glPushMatrix();{
	//glTranslatef(-20,40,105);
		glClipPlane(GL_CLIP_PLANE0,equ);

		glEnable(GL_CLIP_PLANE0);{
			glPushMatrix();{
				glColor3ub(CLR_GOLDEN);
				glTranslatef(0,0.5,height);
				glRotatef(90,1,0,0);
				gluCylinder(quadObj, 3, 3, 2, 10, 10);
				gluCylinder(quadObj, 2.5, 2.5, 2, 10, 10);
				gluDisk(quadObj,2.5,3,10,10);
				glTranslatef(0,0,2);
				gluDisk(quadObj,2.5,3,10,10);
			}glPopMatrix();
		}glDisable(GL_CLIP_PLANE0);
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3ub(CLR_GOLDEN);
		gluCylinder(quadObj, in_radius, in_radius, 17, 10, 10);
	}glPopMatrix();

	glPushMatrix();{
		glColor3ub(CLR_GOLDEN);
		glTranslatef(0,0,10);
			
		gluCylinder(quadObj, 2, 2, 3, 40, 10);
		gluDisk(quadObj,0,2,10,10);
	}glPopMatrix();


	glPushMatrix();{
		glColor3ub(CLR_GOLDEN);
		GLUquadricObj *quadObj = gluNewQuadric();
			gluCylinder(quadObj, 2, 2, 2, 40, 10);
			gluDisk(quadObj,0,2,40,10);
	}glPopMatrix();

	glPushMatrix();{
		glColor3ub(CLR_GOLDEN);
		gluDisk(quadObj,0,2,40,10);
	}glPopMatrix();
}

void drawWindow(float length, float height,float size, struct Color color,struct Color bgcolor) {
	
	
	glPushMatrix();{
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,tex_janala_bg);
		glNormal3f(1.0,0.0,0.0);
			 glBegin(GL_POLYGON);{
				glTexCoord2f(0,0);glVertex3f(-length/2,0,height);
				glTexCoord2f(1,0);glVertex3f(-length/2,0,0);
				glTexCoord2f(1,1);glVertex3f(length/2,0,0);
				glTexCoord2f(0,1);glVertex3f(length/2,0,height);
			}glEnd();
		 glDisable(GL_TEXTURE_2D);

		 glColor3ub(color.r,color.g,color.b);
		//glTranslatef(0,-size/2,0);
		drawRectangle(length,size,size);//nicher ta
		glTranslatef(length/2,0,0);
		drawRectangle(size,size,height);//daner ta
		glTranslatef(-length,0,0);
		drawRectangle(size,size,height);//bamer ta
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(0,0,height);
	double equ[4];

	equ[0] = 0;	//0.x
	equ[1] = 0;	//0.y
	equ[2] = 1;//-1.z
	equ[3] = 0;//30

	glClipPlane(GL_CLIP_PLANE0,equ);

	//now we enable the clip plane

	glEnable(GL_CLIP_PLANE0);{
		glPushMatrix();{
		
			glRotatef(90,1,0,0);
			glColor3ub(color.r,color.g,color.b);
			gluCylinder(quadObj, (length+size)/2, (length+size)/2, size, 20, 20); 
			gluCylinder(quadObj, (length+size)/2-size, (length+size)/2-size, size, 20, 20);
			
			glTranslatef(0,0,size);
			gluDisk(quadObj,(length+size)/2-size,(length+size)/2,20,20);
			glTranslatef(0,0,-size);
			gluDisk(quadObj,(length+size)/2-size,(length+size)/2,20,20);
			glColor3ub(bgcolor.r,bgcolor.g,bgcolor.b);
			glTranslatef(0,0,size/2);
			gluDisk(quadObj,0,(length+size)/2-size,20,20);
		
		}glPopMatrix();

	}glDisable(GL_CLIP_PLANE0);
	}glPopMatrix();
}

void drawWindow2(float length, float height,float size, struct Color color,struct Color bgcolor) {
	
	
	glPushMatrix();{
		 glBegin(GL_POLYGON);{
			glColor3ub(bgcolor.r,bgcolor.g,bgcolor.b);
			glVertex3f(-length/2,0,height);
			glVertex3f(-length/2,0,0);
			glVertex3f(length/2,0,0);
			glVertex3f(length/2,0,height);

		}glEnd();
		 glColor3ub(color.r,color.g,color.b);
		//glTranslatef(0,-size/2,0);
		drawRectangle(length,size,size);//nicher ta
		glTranslatef(0,0,height-1);
		drawRectangle(length,size,size);//uporer ta
		glTranslatef(length/2,0,-height+1);
		drawRectangle(size,size,height);//daner ta
		glTranslatef(-length,0,0);
		drawRectangle(size,size,height);//bamer ta

		glColor3ub(CLR_DARKER2_GRAY);
		glTranslatef(length/4,0,0);
		drawRectangle(size,size,height-1);
		glTranslatef(length/2,0,0);
		drawRectangle(size,size,height-1);

	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(0,0,height);
	double equ[4];

	equ[0] = 0;	//0.x
	equ[1] = 0;	//0.y
	equ[2] = 1;//-1.z
	equ[3] = 0;//30

	glClipPlane(GL_CLIP_PLANE0,equ);

	//now we enable the clip plane

	glEnable(GL_CLIP_PLANE0);{
		glPushMatrix();{
		
			glRotatef(90,1,0,0);
			glColor3ub(color.r,color.g,color.b);
			gluCylinder(quadObj, (length+size)/2, (length+size)/2, size, 20, 20); 
			gluCylinder(quadObj, (length+size)/2-size, (length+size)/2-size, size, 20, 20);
			
			glTranslatef(0,0,size);
			gluDisk(quadObj,(length+size)/2-size,(length+size)/2,20,20);
			glTranslatef(0,0,-size);
			gluDisk(quadObj,(length+size)/2-size,(length+size)/2,20,20);
			glColor3ub(bgcolor.r,bgcolor.g,bgcolor.b);
			glTranslatef(0,0,size/2);
			gluDisk(quadObj,0,(length+size)/2-size,20,20);
		
		}glPopMatrix();

	}glDisable(GL_CLIP_PLANE0);
	}glPopMatrix();
}


void drawWindow1(float length, float height,float size, struct Color color) {
	//circular wall tar jonno
	
	glPushMatrix();{
		 
		 glColor3ub(color.r,color.g,color.b);
		//glTranslatef(0,-size/2,0);
		//drawRectangle(length,size,size);//nicher ta
		glTranslatef(length/2,0,0);
		drawRectangle(size,size,height+size*3);//daner ta
		glTranslatef(-length,0,0);
		drawRectangle(size,size,height+size*3);//bamer ta
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(0,0,height);
	double equ[4];

	equ[0] = 0;	//0.x
	equ[1] = 0;	//0.y
	equ[2] = 1;//-1.z
	equ[3] = 0;//30

	glClipPlane(GL_CLIP_PLANE0,equ);

	//now we enable the clip plane

	glEnable(GL_CLIP_PLANE0);{
		glPushMatrix();{
		
			glRotatef(90,1,0,0);
			glColor3ub(color.r,color.g,color.b);
			gluCylinder(quadObj, (length+size)/2, (length+size)/2, size, 20, 20); 
			gluCylinder(quadObj, (length+size)/2-size, (length+size)/2-size, size, 20, 20);
			
			glTranslatef(0,0,size);
			gluDisk(quadObj,(length+size)/2-size,(length+size)/2,20,20);
			glTranslatef(0,0,-size);
			gluDisk(quadObj,(length+size)/2-size,(length+size)/2,20,20);
			//glColor3ub(bgcolor.r,bgcolor.g,bgcolor.b);
			//glTranslatef(0,0,size/2);
			//gluDisk(quadObj,0,(length+size)/2-size,20,20);
		
		}glPopMatrix();

	}glDisable(GL_CLIP_PLANE0);
	}glPopMatrix();
}

void drawCircularWall() {
	
	float width = 3.5;
	float height = 5;
	float size = 0.5;
	int count= 4;
	
	glPushMatrix();{
		
		for(int i=0;i<=count;i++) {
			drawWindow1(width,height,size,Color(CLR_BRICK));
			glTranslatef(width+size,0,0);
		}

	}glPopMatrix();
	glPushMatrix();{
		glTranslatef((width*count)/2,3,height+size);
		glColor3ub(CLR_DARKER2_GRAY);
		//drawRectangle(20,10,size*2);
	

	}glPopMatrix();
}

void drawCircularBlock(float radius, int slices,float length, float width,float height,struct Color color) {

	glPushMatrix();{

		for(int ii = 0; ii < slices; ii++) 
		{ 
			float theta = 2.0f * 3.1415926f * float(ii) / float(slices);//get the current angle 

			float x = radius * cosf(theta);//calculate the x component 
			float y = radius * sinf(theta);//calculate the y component 

			//glVertex2f(x + cx, y + cy);//output vertex 
		
			glPushMatrix();{
		
				glTranslatef(x,y,0);
				glRotatef(90+(ii*(360/slices)),0,0,1);
				
				glColor3ub(color.r,color.g,color.b);
				drawRectangle(length,width,height);
		
			}glPopMatrix();

		} 
	}glPopMatrix();
}

void drawCircularTriangle(float radius,float inner_radius, int slices,float length,float height,struct Color color) {

	glPushMatrix();{

		for(int ii = 0; ii < slices; ii++) 
		{ 
			float theta = 2.0f * 3.1415926f * float(ii) / float(slices);//get the current angle 

			float x = radius * cosf(theta);//calculate the x component 
			float y = radius * sinf(theta);//calculate the y component 

			//glVertex2f(x + cx, y + cy);//output vertex 
		
			glPushMatrix();{
		
				glTranslatef(x,y,0);
				glRotatef(90+(ii*(360/slices)),0,0,1);
				
				
				glPushMatrix();{
				
				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D,tex_white_wall);
				glNormal3f(1.0,0.0,0.0);
				glBegin(GL_POLYGON);{
					//glColor3ub(color.r,color.g,color.b);
					glTexCoord2f(0,0);glVertex3f(0,(radius-inner_radius),height);
					glTexCoord2f(1,0);glVertex3f(-length/2,0,0);
					glTexCoord2f(1,1);glVertex3f(length/2,0,0);
				}glEnd();
				glDisable(GL_TEXTURE_2D);
				}glPopMatrix();
		//drawRectangle(length,width,height);
			}glPopMatrix();

		} 
	}glPopMatrix();
}

void drawCircularTombJanala(float radius, int slices,float length,float height) {

	glPushMatrix();{

		for(int ii = 0; ii < slices; ii++) 
		{ 
			float theta = 2.0f * 3.1415926f * float(ii) / float(slices);//get the current angle 

			float x = radius * cosf(theta);//calculate the x component 
			float y = radius * sinf(theta);//calculate the y component 

			//glVertex2f(x + cx, y + cy);//output vertex 
		
			glPushMatrix();{
		
				glTranslatef(x,y,0);
				glRotatef(90+(ii*(360/slices)),0,0,1);
				
				
				glPushMatrix();{
					glColor3ub(CLR_WHITE1);
					glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D,tex_tomb_janala);
					glNormal3f(1.0,0.0,0.0);
					glBegin(GL_POLYGON);
						glTexCoord2f(0,0); glVertex3f(-length/2,0,height);
						glTexCoord2f(0,1); glVertex3f(-length/2,0,0);
						glTexCoord2f(1,1); glVertex3f(length/2,0,0);
						glTexCoord2f(1,0); glVertex3f(length/2,0,height);
					glEnd();
					glDisable(GL_TEXTURE_2D);
					
				}glPopMatrix();
		//drawRectangle(length,width,height);
			}glPopMatrix();

		} 
	}glPopMatrix();
}


void drawCircularWindow(float radius, int slices,float width, float height,float size,struct Color color,struct Color bgcolor) {

	glPushMatrix();{

		for(int ii = 0; ii < slices; ii++) 
		{ 
			float theta = 2.0f * 3.1415926f * float(ii) / float(slices);//get the current angle 

			float x = radius * cosf(theta); 
			float y = radius * sinf(theta); 
		
			glPushMatrix();{
		
				glTranslatef(x,y,0);
				glRotatef(90+(ii*(360/slices)),0,0,1);
				drawWindow(width,height,size,color,bgcolor);
		
			}glPopMatrix();

		} 
	}glPopMatrix();
}



void drawCircularWindowForGumbuj(float radius, int slices,float width, float height,float size,struct Color color,struct Color bgcolor) {

	glPushMatrix();{

		for(int ii = 0; ii < slices; ii++) 
		{ 
			float theta = 2.0f * 3.1415926f * float(ii) / float(slices);//get the current angle 

			float x = radius * cosf(theta); 
			float y = radius * sinf(theta); 
		
			glPushMatrix();{
		
				glTranslatef(x,y,0);
				glRotatef(90+(ii*(360/slices)),0,0,1);
				drawWindow(width,height,size,color,bgcolor);
				glTranslatef(width+1,0,0);
				drawWindow(width,height,size,color,bgcolor);
				glTranslatef(-2*(width+1),0,0);
				drawWindow(width,height,size,color,bgcolor);
		
			}glPopMatrix();
		

		} 
	}glPopMatrix();
}


void drawHelanoRectangle(float length,float width,float f_height, float b_height,struct Color color) {
	float X = length/2;
	float Y = width/2;
	float Zf = f_height;
	float Zb = b_height;
	//right
	glBegin(GL_POLYGON);{
			
		glVertex3f(X,-Y,0);
		glVertex3f(X,Y,0);
		glVertex3f(X,Y,Zb);
		glVertex3f(X,-Y,Zf);
				
	}glEnd();
	//left
	glBegin(GL_POLYGON);{
			
		glVertex3f(-X,-Y,0);
		glVertex3f(-X,Y,0);
		glVertex3f(-X,Y,Zb);
		glVertex3f(-X,-Y,Zf);
				
	}glEnd();
	//back
	glBegin(GL_POLYGON);{
			
		glVertex3f(-X,Y,0);
		glVertex3f(X,Y,0);
		glVertex3f(X,Y,Zb);
		glVertex3f(-X,Y,Zb);
				
	}glEnd();
	//front
	glBegin(GL_POLYGON);{
			
		glVertex3f(-X,-Y,0);
		glVertex3f(X,-Y,0);
		glVertex3f(X,-Y,Zf);
		glVertex3f(-X,-Y,Zf);
				
	}glEnd();
	//top
	
		glColor3ub(color.r,color.g,color.b);
		glBegin(GL_POLYGON);{
			
			glVertex3f(-X,-Y,Zf);
			glVertex3f(X,-Y,Zf);
			glVertex3f(X,Y,Zb);
			glVertex3f(-X,Y,Zb);
				
		}glEnd();
	
}

void drawHelanoRectangleTex(float length,float width,float f_height, float b_height,struct Color color,GLuint texid) {
	float X = length/2;
	float Y = width/2;
	float Zf = f_height;
	float Zb = b_height;
	//right
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		glTexCoord2f(0,0);glVertex3f(X,-Y,0);
		glTexCoord2f(1,0);glVertex3f(X,Y,0);
		glTexCoord2f(1,1);glVertex3f(X,Y,Zb);
		glTexCoord2f(0,1);glVertex3f(X,-Y,Zf);
				
	}glEnd();
	glDisable(GL_TEXTURE_2D);
	//left
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		glTexCoord2f(0,0);glVertex3f(-X,-Y,0);
		glTexCoord2f(1,0);glVertex3f(-X,Y,0);
		glTexCoord2f(1,1);glVertex3f(-X,Y,Zb);
		glTexCoord2f(0,1);glVertex3f(-X,-Y,Zf);
				
	}glEnd();
	glDisable(GL_TEXTURE_2D);
	//back
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		glTexCoord2f(0,0);glVertex3f(-X,Y,0);
		glTexCoord2f(1,0);glVertex3f(X,Y,0);
		glTexCoord2f(1,1);glVertex3f(X,Y,Zb);
		glTexCoord2f(0,1);glVertex3f(-X,Y,Zb);
				
	}glEnd();
	glDisable(GL_TEXTURE_2D);
	//front
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		glTexCoord2f(0,0);glVertex3f(-X,-Y,0);
		glTexCoord2f(1,0);glVertex3f(X,-Y,0);
		glTexCoord2f(1,1);glVertex3f(X,-Y,Zf);
		glTexCoord2f(0,1);glVertex3f(-X,-Y,Zf);
				
	}glEnd();
	glDisable(GL_TEXTURE_2D);
	//top
	
		//glColor3ub(color.r,color.g,color.b);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,tex_tupi);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);{
			
			glTexCoord2f(0,0);glVertex3f(-X,-Y,Zf);
			glTexCoord2f(1,0);glVertex3f(X,-Y,Zf);
			glTexCoord2f(1,1);glVertex3f(X,Y,Zb);
			glTexCoord2f(0,1);glVertex3f(-X,Y,Zb);
				
		}glEnd();
		glDisable(GL_TEXTURE_2D);
	
}


void drawcurcularRectangle(float length,float width,float front_ext,float fheight,float bheight) {

	float X = length/2;

	float X1 = width;//middle
	float X2 = width-front_ext;//top
	float X3 = width+front_ext;//bottom
	float Y = length;
	float Zf = fheight/2;
	float Zb= bheight/2;

	float	radius = bheight/2;
	float slices = 10;
	glPushMatrix();{
		//glTranslatef(0,-width/2,0);
		glColor3ub(CLR_BRICK3);
		//glColor3ub(CLR_DARKER2_GRAY);
		//drawRectangle(length,X1,Zb);
	
	//right
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex_redwall);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		glTexCoord2f(0,0);glVertex3f(X,-X1,Zf);
		glTexCoord2f(1,0);glVertex3f(X,-X3,0);
		glTexCoord2f(1,1);glVertex3f(X,0,0);
		glTexCoord2f(0,1);glVertex3f(X,0,Zf);
				
	}glEnd();
	glDisable(GL_TEXTURE_2D);
	//left
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex_redwall);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		glTexCoord2f(0,0);glVertex3f(-X,-X1,Zf);
		glTexCoord2f(1,0);glVertex3f(-X,-X3,0);
		glTexCoord2f(1,1);glVertex3f(-X,0,0);
		glTexCoord2f(0,1);glVertex3f(-X,0,Zf);
				
	}glEnd();
	glDisable(GL_TEXTURE_2D);
	
	//front
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex_redwall);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		glTexCoord2f(0,0);glVertex3f(-X,-X1,Zf);
		glTexCoord2f(1,0);glVertex3f(-X,-X3,0);
		glTexCoord2f(1,1);glVertex3f(X,-X3,0);
		glTexCoord2f(0,1);glVertex3f(X,-X1,Zf);
				
	}glEnd();
	glDisable(GL_TEXTURE_2D);
	//top
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex_redwall);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		glTexCoord2f(0,0);glVertex3f(-X,0,Zf);
		glTexCoord2f(1,0);glVertex3f(-X,-X1,Zf);
		glTexCoord2f(1,1);glVertex3f(X,-X1,Zf);
		glTexCoord2f(0,1);glVertex3f(X,0,Zf);
				
	}glEnd();
	glDisable(GL_TEXTURE_2D);
	}glPopMatrix();

	//end of bottom rec
	glPushMatrix();{
		glTranslatef(-length/2,0,Zf);
		glRotatef(90,0,0,1);
	glBegin(GL_POLYGON);{
			
		float x,z;
		float x1,z1;
		glColor3ub(CLR_WHITE);
		
		glVertex3f(-X2,0,Zf);
		glVertex3f(-X1,0,0);
	
		for(int ii = slices; ii >=0; ii--) 
			{ 
				float theta = (3.1415926f*.5)+(2.0f * 3.1415926f * float(ii) / float(slices*4));
				x = radius * cosf(theta);
				z = radius * sinf(theta);
				
				glVertex3f(x,0,z);

				if(ii==0) {
					x1=x;
					z1=z;}
			}
		
		glVertex3f(x,0,z+3);
		
	}glEnd();

	glBegin(GL_POLYGON);{
			
		float x,z;
		float x1,z1;
		glColor3ub(CLR_WHITE);
		glVertex3f(-X2,-Y,Zf);
		glVertex3f(-X1,-Y,0);
	
		for(int ii = slices; ii >=0; ii--) 
			{ 
				float theta = (3.1415926f*.5)+(2.0f * 3.1415926f * float(ii) / float(slices*4));
				x = radius * cosf(theta);
				z = radius * sinf(theta);
				
				glVertex3f(x,-Y,z);

				if(ii==0) {
					x1=x;
					z1=z;}
			}
		
		glVertex3f(x,-Y,z+3);
		
	}glEnd();
	
	//front
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex_gray_bakano_rec);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
	
		glVertex3f(-X2,0,Zf);
		glVertex3f(-X1,0,0);
		glVertex3f(-X1,-Y,0);
		glVertex3f(-X2,-Y,Zf);

	}glEnd();
	glDisable(GL_TEXTURE_2D);

	//top
	glBegin(GL_POLYGON);{
		glColor3ub(CLR_DARKER_GRAY);
		glVertex3f(0,0,Zb+3);
		glVertex3f(-X2,0,Zf);
		glVertex3f(-X2,-Y,Zf);
		glVertex3f(0,-Y,Zb+3);

	}glEnd();
	
	
	}glPopMatrix();


}


void drawFrontCornerPiller(struct Color color) {
	float base_width = 6.4;
	float base_height = 18;

	float radius = 5/2;
	
	//base
	glPushMatrix();{
		glColor3ub(CLR_WHITE);
		drawRectangleWithTexture(base_width,base_width,base_height,tex_white_wall);
	}glPopMatrix();

	glPushMatrix();{
		glColor3ub(CLR_WHITE);
		glTranslatef(0,0,base_height);
		gluCylinder(quadObj, base_width/2, radius, 8, 6, 30);
		drawCircularTriangle(base_width/2, radius,6,4,8,Color(CLR_WHITE));
	}glPopMatrix();
	
	glPushMatrix();{
		
		glTranslatef(0,0,base_height+8);
		glColor3ub(color.r,color.g,color.b);
		
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, tex_white_wall);
			gluQuadricNormals(quadObj,GLU_SMOOTH);
			gluQuadricTexture(quadObj, GLU_TRUE);
			gluCylinder(quadObj, radius, radius, base_height, 30, 30);
		glDisable(GL_TEXTURE_2D);
		 
		//khondo khondo cylinder
		glColor3ub(CLR_WHITE);
		glTranslatef(0,0,base_height);
		for(int i=1;i<=4;i++) {
			if(i==4) {
				glColor3ub(255,255,255);

				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D, tex_white_wall);
					gluQuadricNormals(quadObj,GLU_SMOOTH);
					gluQuadricTexture(quadObj, GLU_TRUE);
					gluCylinder(quadObj, radius+(i/2), radius+(i/2), 2, 30, 30);
				glDisable(GL_TEXTURE_2D);
				
			}
			else {
				glColor3ub(CLR_WHITE);

				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D, tex_white_wall);
					gluQuadricNormals(quadObj,GLU_SMOOTH);
					gluQuadricTexture(quadObj, GLU_TRUE);
					gluCylinder(quadObj, radius+(i/2), radius+(i/2), 1, 30, 30);
				glDisable(GL_TEXTURE_2D);
				
			}
			
			glTranslatef(0,0,1);
			gluDisk(quadObj,radius,radius+(i/2),10,10);
		}
		
	}glPopMatrix();
		
	glPushMatrix();{
		glColor3ub(CLR_DARK_GRAY);
		glTranslatef(0,0,49);
		glColor3ub(color.r,color.g,color.b);
		
		glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, tex_white_wall);
			gluQuadricNormals(quadObj,GLU_SMOOTH);
			gluQuadricTexture(quadObj, GLU_TRUE);
			gluCylinder(quadObj, radius, radius, 8, 30, 30);
		glDisable(GL_TEXTURE_2D);
		
		glTranslatef(0,0,8);
		glColor3ub(CLR_DARKER2_GRAY);
		
		gluCylinder(quadObj, radius, 0, 8, 5, 1);
		glTranslatef(0,0,0.5);
		glColor3ub(CLR_BLACK);
		
		glutWireCone(radius,8,12,1);
		glTranslatef(0,0,7);
		glPushMatrix();{
			glScalef(0.3,0.5,0.4);
			minar_top();
		}glPopMatrix();
	}glPopMatrix();

}

void drawFrontCornerPiller2(struct Color color) {
	float base_width = 6.4;
	float base_height = 18;

	float radius = 5/2;
	
	//base
	glPushMatrix();{
		glColor3ub(CLR_WHITE);
		drawRectangleWithTexture(base_width,base_width,base_height-5,tex_white_wall);
	}glPopMatrix();

	glPushMatrix();{
		glColor3ub(CLR_WHITE);
		glTranslatef(0,0,base_height-5);
		
		gluCylinder(quadObj, base_width/2, radius, 8, 6, 30); 
		drawCircularTriangle(base_width/2, radius,6,4,8,Color(CLR_DARK_GRAY));
	}glPopMatrix();
	
	glPushMatrix();{
		
		glTranslatef(0,0,base_height+8-5);
		glColor3ub(CLR_WHITE);
		glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, tex_white_wall);
			gluQuadricNormals(quadObj,GLU_SMOOTH);
			gluQuadricTexture(quadObj, GLU_TRUE);
			gluCylinder(quadObj, radius, radius, 5, 30, 30);
		glDisable(GL_TEXTURE_2D);
		
		glTranslatef(0,0,5);
		glColor3ub(color.r,color.g,color.b);
		
		glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, tex_redwall);
			gluQuadricNormals(quadObj,GLU_SMOOTH);
			gluQuadricTexture(quadObj, GLU_TRUE);
			gluCylinder(quadObj, radius, radius, base_height, 30, 30); 
		glDisable(GL_TEXTURE_2D);
		
		//khondo khondo cylinder
		glColor3ub(CLR_WHITE);
		glTranslatef(0,0,base_height);
		for(int i=1;i<=4;i++) {
			if(i==4) {
				glColor3ub(255,255,255);
				glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, tex_white_wall);
					gluQuadricNormals(quadObj,GLU_SMOOTH);
					gluQuadricTexture(quadObj, GLU_TRUE);
					gluCylinder(quadObj, radius+(i/2), radius+(i/2), 2, 30, 30);
				glDisable(GL_TEXTURE_2D);
				
			}
			else {
				glColor3ub(CLR_BRICK3);
				glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, tex_redwall);
					gluQuadricNormals(quadObj,GLU_SMOOTH);
					gluQuadricTexture(quadObj, GLU_TRUE);
					gluCylinder(quadObj, radius+(i/2), radius+(i/2), 1, 30, 30);
				glDisable(GL_TEXTURE_2D);
				
			}
			
			glTranslatef(0,0,1);
			gluDisk(quadObj,radius,radius+(i/2),10,10);
		}
		
	}glPopMatrix();
		
	glPushMatrix();{
		glColor3ub(CLR_DARK_GRAY);
		glTranslatef(0,0,49);
		glColor3ub(color.r,color.g,color.b);
		
		glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, tex_redwall);
			gluQuadricNormals(quadObj,GLU_SMOOTH);
			gluQuadricTexture(quadObj, GLU_TRUE);
			gluCylinder(quadObj, radius, radius, 7, 30, 30);
		glDisable(GL_TEXTURE_2D);
		
		glTranslatef(0,0,7);
		
		glColor3ub(CLR_WHITE);
		gluCylinder(quadObj, radius, radius, 1, 10, 30);
		glTranslatef(0,0,1);
		
		glColor3ub(CLR_DARKER_GRAY);
		gluCylinder(quadObj, radius, 0, 8, 5, 1);
		glTranslatef(0,0,0.5);
		glColor3ub(CLR_BLACK);
		glutWireCone(radius,8,12,1);
		glTranslatef(0,0,7);
		
		glPushMatrix();{
			glScalef(0.3,0.5,0.4);
			minar_top();
		}glPopMatrix();
	}glPopMatrix();

}

void drawcurcularRectangleDouble(float length,float width,float front_ext,float fheight,float bheight) {

	float X = length/2;

	float X1 = width;//middle
	float X2 = width-front_ext;//top
	float X3 = width+front_ext;//bottom
	float Y = length;
	float Zf = fheight/2;
	float Zb= bheight/2;

	float	radius = bheight/2+2;
	float slices = 10;

	glPushMatrix();{
		glTranslatef(-length/2,0,0);
		glRotatef(90,0,0,1);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex_graywall);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		float x,z;
		float x1,z1;
		glColor3ub(CLR_WHITE);
		
		glVertex3f(-X2,0,Zf);
		glVertex3f(-X1,0,0);
	
		for(int ii = slices; ii >=0; ii--) 
			{ 
				float theta = (3.1415926f*.5)+(2.0f * 3.1415926f * float(ii) / float(slices*4));
				x = radius * cosf(theta);
				z = radius * sinf(theta);
				
				glTexCoord2f(0,0);glVertex3f(x,0,z);

				if(ii==0) {
					x1=x;
					z1=z;}
			}
		
		glVertex3f(x,0,z+3);
		
	}glEnd();
	glDisable(GL_TEXTURE_2D);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex_graywall);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		float x,z;
		float x1,z1;
		glColor3ub(CLR_WHITE);
		glVertex3f(-X2,-Y,Zf);
		glVertex3f(-X1,-Y,0);
	
		for(int ii = slices; ii >=0; ii--) 
			{ 
				float theta = (3.1415926f*.5)+(2.0f * 3.1415926f * float(ii) / float(slices*4));
				x = radius * cosf(theta);
				z = radius * sinf(theta);
				
				glTexCoord2f(0,0);glVertex3f(x,-Y,z);

				if(ii==0) {
					x1=x;
					z1=z;}
			}
		
		glVertex3f(x,-Y,z+3);
		
	}glEnd();
	glDisable(GL_TEXTURE_2D);
	
	//front
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex_graywall);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
	
		glTexCoord2f(0,0);glVertex3f(-X2,0,Zf);
		glTexCoord2f(1,0);glVertex3f(-X1,0,0);
		glTexCoord2f(1,1);glVertex3f(-X1,-Y,0);
		glTexCoord2f(0,1);glVertex3f(-X2,-Y,Zf);

	}glEnd();
	glDisable(GL_TEXTURE_2D);

	//top
	glBegin(GL_POLYGON);{
		glColor3ub(CLR_DARKER_GRAY);
		glVertex3f(0,0,Zb+5);
		glVertex3f(-X2,0,Zf);
		glVertex3f(-X2,-Y,Zf);
		glVertex3f(0,-Y,Zb+5);

	}glEnd();
	
	
	}glPopMatrix();

	//2nd one on top
	glPushMatrix();{
		glTranslatef(-length/2,0,Zf);
		glRotatef(90,0,0,1);
	
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex_graywall);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		float x,z;
		float x1,z1;
		glColor3ub(CLR_WHITE);
		
		glVertex3f(-X2,0,Zf);
		glVertex3f(-X1,0,0);
	
		for(int ii = slices; ii >=0; ii--) 
			{ 
				float theta = (3.1415926f*.5)+(2.0f * 3.1415926f * float(ii) / float(slices*4));
				x = radius * cosf(theta);
				z = radius * sinf(theta);
				
				glTexCoord2f(0,0);glVertex3f(x,0,z);

				if(ii==0) {
					x1=x;
					z1=z;}
			}
		
		glVertex3f(x,0,z+3);
		
	}glEnd();
	glDisable(GL_TEXTURE_2D);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex_graywall);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
			
		float x,z;
		float x1,z1;
		glColor3ub(CLR_WHITE);
		glVertex3f(-X2,-Y,Zf);
		glVertex3f(-X1,-Y,0);
	
		for(int ii = slices; ii >=0; ii--) 
			{ 
				float theta = (3.1415926f*.5)+(2.0f * 3.1415926f * float(ii) / float(slices*4));
				x = radius * cosf(theta);
				z = radius * sinf(theta);
				
				glTexCoord2f(0,0);glVertex3f(x,-Y,z);

				if(ii==0) {
					x1=x;
					z1=z;}
			}
		
		glVertex3f(x,-Y,z+3);
		
	}glEnd();
	glDisable(GL_TEXTURE_2D);
	
	//front
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex_graywall);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);{
	
		glTexCoord2f(0,0);glVertex3f(-X2,0,Zf);
		glTexCoord2f(1,0);glVertex3f(-X1,0,0);
		glTexCoord2f(1,1);glVertex3f(-X1,-Y,0);
		glTexCoord2f(0,1);glVertex3f(-X2,-Y,Zf);

	}glEnd();
	glDisable(GL_TEXTURE_2D);

	//top
	glBegin(GL_POLYGON);{
		glColor3ub(CLR_DARKER_GRAY);
		glVertex3f(0,0,Zb+5);
		glVertex3f(-X2,0,Zf);
		glVertex3f(-X2,-Y,Zf);
		glVertex3f(0,-Y,Zb+5);

	}glEnd();
	
	
	}glPopMatrix();

	

}

void drawTupi(float radius, int slices, bool isSolid) {

	/// the cutting plane equation: z = 30
	/// we will keep the points with
	//		z <= 30
	//OR	0.x + 0.y + 1.z - 30 <= 0
	//OR	0.x + 0.y - 1.z + 30 >= 0	//// standard format:: ax + by + cz + d >= 0
	
	double equ[4];

	equ[0] = 0;	//0.x
	equ[1] = 0;	//0.y
	equ[2] = 1;//-1.z
	equ[3] = 0;//30

	glClipPlane(GL_CLIP_PLANE0,equ);

	//now we enable the clip plane

	glEnable(GL_CLIP_PLANE0);{
		glPushMatrix();{
		
			if(isSolid) {
					glEnable(GL_TEXTURE_2D);
						glBindTexture(GL_TEXTURE_2D, tex_tupi);
						gluQuadricNormals(quadObj,GLU_SMOOTH);
						gluQuadricTexture(quadObj, GLU_TRUE);
						glutSolidSphere(radius, slices, slices);
					glDisable(GL_TEXTURE_2D);
				//glutSolidSphere(radius, slices, slices);
			}
			else {
					glEnable(GL_TEXTURE_2D);
						glBindTexture(GL_TEXTURE_2D, tex_tupi);
						gluQuadricNormals(quadObj,GLU_SMOOTH);
						gluQuadricTexture(quadObj, GLU_TRUE);
						glutSolidSphere(radius, slices, slices);
					glDisable(GL_TEXTURE_2D);
				//glColor3ub(120,120,120);
				//glutSolidSphere(radius, slices, slices);
				glColor3ub(0,0,0);
				glTranslatef(0,0,0.2);
				glutWireSphere(radius, slices, slices);
			}
		
		}glPopMatrix();

	}glDisable(GL_CLIP_PLANE0);

}

void drawOuterGumbujj(float height,float radius, float slices,bool isGumbujSolid) {

	glPushMatrix();{

		//glTranslatef(0,0,legheight+1);
		glPushMatrix();{
			glColor3ub(CLR_WHITE);
		glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, tex_graywall); 
			gluQuadricNormals(quadObj,GLU_SMOOTH);
			gluQuadricTexture(quadObj, GLU_TRUE);
			gluCylinder(quadObj, radius, radius, height, slices, 30); 
		glDisable(GL_TEXTURE_2D);

			glTranslatef(0,0,height);
			glColor3ub(CLR_DARKER2_GRAY);
			gluDisk(quadObj,0,radius,slices,10);
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,0,1);
			drawCircularWindowForGumbuj(radius-1,6,1.5,height/4-0.5,0.2,Color(CLR_BLACK),Color(CLR_LIGHTER_GRAY));
			glTranslatef(0,0,height/3);
			drawCircularWindowForGumbuj(radius-1,6,1.5,height/4-0.5,0.2,Color(CLR_BLACK),Color(CLR_LIGHTER_GRAY));
			glTranslatef(0,0,height/3-0.5);
			drawCircularWindowForGumbuj(radius-1,6,1.5,height/4-0.5,0.2,Color(CLR_BLACK),Color(CLR_LIGHTER_GRAY));
		}glPopMatrix();
		
		glTranslatef(0,0,height);
		//gluDisk(quadObj,5,radius,slices,30);
		
		glColor3ub(CLR_DARKER_GRAY);
		drawTupi(radius-1,10,isGumbujSolid);
		
	}glPopMatrix();
}
void drawOuterGumbujj2(float height,float radius, float slices,bool isGumbujSolid) {

	glPushMatrix();{

		//glTranslatef(0,0,legheight+1);
		glColor3ub(CLR_DARK_GRAY);
		gluCylinder(quadObj, radius, radius, height, slices, 30); 
		
		glPushMatrix();{
			glTranslatef(0,0,1);
			drawCircularWindow(radius,6,2,height/4,0.4,Color(CLR_BLACK),Color(CLR_LIGHTER_GRAY));
			glTranslatef(0,0,height/2);
			drawCircularWindow(radius,6,2,height/4,0.4,Color(CLR_BLACK),Color(CLR_LIGHTER_GRAY));
			
		}glPopMatrix();
		
		glTranslatef(0,0,height);
		//gluDisk(quadObj,5,radius,slices,30);
		
		glColor3ub(CLR_DARKER_GRAY);
		drawTupi(radius-1,10,isGumbujSolid);
		
	}glPopMatrix();
}

void drawcenterObject() {
	float height = 45;
	float length = 30;
	float width = 30;

	glPushMatrix();{
		glColor3ub(200,200,200);
		drawRectangleWithTexture(length,width,height,tex_brick);
		glColor3ub(CLR_DARKER_GRAY);
		glTranslatef(0,0,height);
		drawRectangle(length,width,2);
		glTranslatef(0,0,2);
		
		glColor3ub(CLR_DARKER_GRAY);
		drawTupi(length/2,20,false);
		
		glPushMatrix();{
			glTranslatef(0,0,length/2);
			
			glScalef(0.6,0.6,0.3);
			glColor3ub(CLR_GOLDEN);
			gluCylinder(quadObj,4,2,4,10,10);
			glTranslatef(0,0,4);
			minar_top();
		}glPopMatrix();
		
		
		drawCircularTombJanala(length/2,25,4,7);
		glColor3ub(CLR_BLACK);
		//drawCircularBlock(length/2-0.2,30,3,0.5,3,Color(CLR_WHITE));
		//drawCircularBlock(length/2,30,1.5,0.5,3,Color(CLR_BLACK));
		glTranslatef(0,0,3);
		//glutSolidTorus(0.5,length/2+1,20,20);
		glTranslatef(0,0,0.5);
		
		//drawCircularBlock(length/2,30,1,1.5,1,Color(CLR_BLACK));
	
	}glPopMatrix();
}

void drawleftright1() {
	
	float center_obj_length = 30;
	float front_width = 14;
	float front_height = 15;
	
	float left_cube_length = 6.5;
		
	glColor3ub(CLR_BRICK3);
		
	glTranslatef(0,-(front_width)/2,0);
	drawRectangle(center_obj_length,front_width,front_height);

	//janala gula bamer
	glPushMatrix();{
		glTranslatef(-4,-(front_width)/2+1,front_height);
		drawWindow(1.3,4,0.4,Color(CLR_BRICK3),Color(CLR_BLACK));
		glTranslatef(3,0,0);
		drawWindow(1.3,2.5,0.2,Color(CLR_BRICK3),Color(CLR_BLACK));
		glTranslatef(-6,0,0);
		drawWindow(1.3,2.5,0.2,Color(CLR_BRICK3),Color(CLR_BLACK));
		//nicher arektu boro gula
		glTranslatef(0,-1.2,-8);
		drawWindow(2,5,0.4,Color(CLR_WHITE),Color(CLR_DARKER_GRAY));
		glTranslatef(3,0,0);
		drawWindow(2,5,0.4,Color(CLR_WHITE),Color(CLR_DARKER_GRAY));
		//daner gula
		glTranslatef(8,0,0);
		drawWindow(2,5,0.4,Color(CLR_WHITE),Color(CLR_DARKER_GRAY));
		glTranslatef(3,0,0);
		drawWindow(2,5,0.4,Color(CLR_WHITE),Color(CLR_DARKER_GRAY));
		glTranslatef(12,0,0);
		drawWindow(2,5,0.4,Color(CLR_WHITE),Color(CLR_DARKER_GRAY));
		glTranslatef(3,0,0);
		drawWindow(1.5,3,0.4,Color(CLR_WHITE),Color(CLR_DARKER_GRAY));
		glTranslatef(3,0,0);
		drawWindow(2,5,0.4,Color(CLR_WHITE),Color(CLR_DARKER_GRAY));
		}glPopMatrix();
	//janala gula daner
	glPushMatrix();{
		glTranslatef(4,-(front_width)/2+1,front_height);
		drawWindow(1.3,4,0.4,Color(CLR_BRICK3),Color(CLR_BLACK));
		glTranslatef(3,0,0);
		drawWindow(1.3,2.5,0.2,Color(CLR_BRICK3),Color(CLR_BLACK));
		glTranslatef(-6,0,0);
		drawWindow(1.3,2.5,0.2,Color(CLR_BRICK3),Color(CLR_BLACK));
		}glPopMatrix();
	
	//majhe half circle ta
	glPushMatrix();{
		glColor3ub(CLR_BLACK);
		glTranslatef(0,(front_width)/2,front_height+7);
		glRotatef(90,1,0,0);
		glColor3ub(CLR_DARKER_GRAY);
		gluCylinder(quadObj, 20/2, 20/2, 1, 30, 30);
		gluCylinder(quadObj, 20/2-1, 20/2-1, 1, 30, 30);
		glTranslatef(0,0,1);
		gluDisk(quadObj, 20/2-1,20/2 , 30, 30);

	}glPopMatrix();
	//pechoner janala gulan
	
	glPushMatrix();{//nicher soman 5 ta
		glTranslatef(-20/2+1,(front_width)/2,front_height+4);
		for(int i=0;i<5;i++) {
			glTranslatef(3,-0.2,0);
			drawWindow(1.3,3,0.4,Color(CLR_BLACK),Color(CLR_LIGHTER_GRAY));
		}

	}glPopMatrix();
	glPushMatrix();{//uporer boro choto 5 ta
		glTranslatef(-20/2+1,(front_width)/2,front_height+9);
		for(int i=1;i<=5;i++) {
			glTranslatef(3,-0.2,0);
			if(i==1 || i==5)
				drawWindow(1.3,3,0.4,Color(CLR_WHITE),Color(CLR_BLACK));
			else if(i==2 || i==4)
				drawWindow(1.3,4,0.4,Color(CLR_WHITE),Color(CLR_BLACK));
			else
				drawWindow(1.3,5,0.4,Color(CLR_WHITE),Color(CLR_BLACK));
		}

	}glPopMatrix();

	glPushMatrix();{
		glColor3ub(10,100,100);
		glTranslatef(0,0,front_height);
		glPushMatrix();{
			glColor3ub(CLR_LIGHT_GRAY);
			drawRectangle(center_obj_length,front_width,1);
			glColor3ub(CLR_DARK_GRAY);
			glTranslatef(-17/2,0,0);
			glRotatef(90,0,1,0);
			gluCylinder(quadObj, 5, 5, 17, 30, 30); //gluCylinder(quadObj, base, top, height, slices, stacks)
		}glPopMatrix();
			
		glPushMatrix();{
			glTranslatef(-3.5,0,5);
			drawTupi(2.5,8,false);
			glTranslatef(7,0,0);
			drawTupi(2.5,8,false);
			
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(-4,0,2);
			glRotatef(90,1,0,0);
			glColor3ub(CLR_DARKER_GRAY);
			gluCylinder(quadObj, 4, 4, 7, 30, 30); //gluCylinder(quadObj, base, top, height, slices, stacks)
				
			glPushMatrix();{
				glColor3ub(CLR_BRICK);
				glTranslatef(0,0,7-1);
				gluDisk(quadObj, 0,4 , 30, 30);

			}glPopMatrix();
				
			glTranslatef(8,0,0);
			glColor3ub(CLR_DARKER_GRAY);
			gluCylinder(quadObj, 4, 4, 7, 30, 30);
			glPushMatrix();{
				glColor3ub(CLR_BRICK);
				glTranslatef(0,0,7-1);
				gluDisk(quadObj, 0,4 , 30, 30);
			}glPopMatrix();
			
		}glPopMatrix();
			
	}glPopMatrix();
}

void drawleftright2() {
	//left
	float front_width = 14;
	float left_cube_length = 6.5;
	glPushMatrix();{
			
		glColor3ub(CLR_WHITE);
		drawRectangleWithTexture(left_cube_length,front_width,5.00,tex_brick);
		glTranslatef(0,0,5.00);
		glColor3ub(CLR_DARKER2_GRAY);
		drawRectangleWithTexture(left_cube_length,front_width-1,0.5,tex_brick);
		glTranslatef(0,0,0.5);
		
		glColor3ub(CLR_WHITE);
		drawRectangleWithTexture(left_cube_length-1,front_width,7.0,tex_brick);
		glTranslatef(0,0,7.00);
		glColor3ub(CLR_DARKER2_GRAY);
		drawRectangleWithTexture(left_cube_length-1,front_width-1,0.5,tex_brick);
		glTranslatef(0,0,0.5);
			
		glColor3ub(CLR_WHITE);
		drawRectangleWithTexture(left_cube_length-2,front_width,7.0,tex_brick);
		glTranslatef(0,0,7.00);
		glColor3ub(CLR_DARKER2_GRAY);
		drawRectangleWithTexture(left_cube_length-2,front_width-1,0.5,tex_brick);
		glTranslatef(0,0,0.5);
		
		
			
		//top one
		glPushMatrix();{

			glColor3ub(CLR_WHITE);
			glTranslatef(0,-(front_width/4),0);
			drawRectangleWithTexture(left_cube_length-3,front_width/2,5.5,tex_brick);
				
			glTranslatef(0,front_width/4,5.5);
			glRotatef(90,1,0,0);
			glColor3ub(CLR_DARKER2_GRAY);
			gluCylinder(quadObj, (left_cube_length-3)/2, (left_cube_length-3)/2, front_width/2, 30, 30); 
			gluCylinder(quadObj, (left_cube_length-3)/2-0.5, (left_cube_length-3)/2-0.5, front_width/2+1, 30, 30); 
			glPushMatrix();{
				glColor3ub(CLR_BRICK);
				glTranslatef(0,0,front_width/2);
				gluDisk(quadObj, 0,(left_cube_length-3)/2 , 30, 30); 
				glTranslatef(0,0,0.5);
				gluDisk(quadObj, 0,(left_cube_length-3)/2-0.5 , 30, 30); 
			}glPopMatrix();
		}glPopMatrix();
		//top one end
			
	}glPopMatrix();
}
void drawleftright3() {
	float radius = 8;
	float height = 14;

	glPushMatrix();{
		
		drawOuterGumbujj(height,radius,6,false);
		
		glTranslatef(radius+7,radius,0);
		drawOuterGumbujj(height,radius,6,false);
		
		glTranslatef(radius-5,-radius-10,0);
		drawOuterGumbujj(height,radius,6,false);
	}glPopMatrix();
}
void drawleftright() {
	float width = 14;
	float left_cube_length = 6.5;
	float length = 30;
	float height = 15;

	glPushMatrix();{
		//glTranslatef(0,-width/2,0);
		drawleftright1();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(-(length-left_cube_length)/2,-width/2,height);
		drawleftright2();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef((length-left_cube_length)/2,-width/2,height);
		drawleftright2();
	}glPopMatrix();
	
}

void drawleft() {
	float width = 14;
	float length = 30;
	float height = 15;

	float helano_rec_width = 15;

	glPushMatrix();{
		drawleftright();
	}glPopMatrix();

	glPushMatrix();{
		//middle
		glTranslatef(0,-(width+helano_rec_width/2),0);
		glColor3ub(CLR_WHITE);
		drawHelanoRectangleTex(4.25,helano_rec_width-6,height-4,height,Color(CLR_DARKER_GRAY),tex_brick);
		//right
		glPushMatrix();{
			glTranslatef((length-6.5)/2,0,0);
			glColor3ub(CLR_WHITE);
			drawHelanoRectangleTex(6.5,helano_rec_width,height-4,height,Color(CLR_DARKER_GRAY),tex_brick);
		}glPopMatrix();
		//left
		//right
		glPushMatrix();{
			glTranslatef(-(length-6.5)/2,0,0);
			glColor3ub(CLR_WHITE);
			drawHelanoRectangleTex(6.5,helano_rec_width,height-4,height,Color(CLR_DARKER_GRAY),tex_brick);
		}glPopMatrix();


	}glPopMatrix();
}

void drawright() {
	float width = 14;
	float length = 30;
	float height = 15;

	float helano_rec_width = 15;

	glPushMatrix();{
		drawleftright();
	}glPopMatrix();

	glPushMatrix();{
		//middle
		glTranslatef(0,-(width+helano_rec_width/2),0);
		glColor3ub(CLR_WHITE);
		drawHelanoRectangleTex(6.0,helano_rec_width+6,height-4,height,Color(CLR_DARKER_GRAY),tex_graywall);
		//right
		glPushMatrix();{
			glTranslatef((length-6.5)/2,0,0);
			glColor3ub(CLR_WHITE);
			drawHelanoRectangleTex(6.5,helano_rec_width,height-4,height,Color(CLR_DARKER_GRAY),tex_graywall);
		}glPopMatrix();
		//left
		glPushMatrix();{
			glTranslatef(-(length-6.5)/2,0,0);
			glColor3ub(CLR_WHITE);
			drawHelanoRectangleTex(6.5,helano_rec_width,height-4,height,Color(CLR_DARKER_GRAY),tex_graywall);
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,-width*2,0);
			drawleftright3();
		}glPopMatrix();
		//right side er akabaka rectangle gula
		//glTranslatef(0,(helano_rec_width/2-11.27/2),0);
		glPushMatrix();{
			//length=16.20
			//width=11.27
			//back_height = 8
			//front ext = 4
			glTranslatef((length+16.20)/2,-4,0);
			glColor3ub(CLR_BRICK);
			drawHelanoRectangle(16.20,11.27,8-4,8,Color(CLR_DARKER_GRAY));
			//uporer block ta
			glColor3ub(CLR_BRICK);
			glTranslatef(0,5,0);
			glRotatef(90,0,0,1);
			drawHelanoRectangle(11.7,4.66,20-5,20,Color(CLR_DARK_GRAY));
			glPushMatrix();{
				glTranslatef(11.7/2,-(4.66+4)/2,0);
				glColor3ub(CLR_DARK_GRAY);
				drawHelanoRectangle(4,4,21,21,Color(CLR_DARKER_GRAY));
			}glPopMatrix();
			glTranslatef(0,3.68,0);
			glColor3ub(CLR_BRICK);
			drawHelanoRectangle(11.7,2.71,21-1,21,Color(CLR_DARK_GRAY));

		}glPopMatrix();


	}glPopMatrix();
}

void drawMiddleStructure() {
	
	float height = 45;
	float length = 30;
	float width = 30;

	glPushMatrix();{
		glColor3ub(157,157,154);
		drawRectangle(length,width,height);
		glColor3ub(0,0,0);
		glTranslatef(0,0,height);
		drawRectangle(length,width,2);
		//glTranslatef(0,0,2);
		
		glColor3ub(120,120,120);
		drawTupi(length/2,10,true);
	
		glColor3ub(0,0,0);
		glTranslatef(0,0,0.2);
		drawTupi(length/2,10,false);
	}glPopMatrix();
	
	//front cube
	float front_width = 14;
	float front_height = 15;
	
	float left_cube_length = 6.5;
	glPushMatrix();{
		glColor3ub(120,100,100);
		glTranslatef(0,-(length+front_width)/2,0);
		drawRectangle(length,front_width,front_height);
		glPushMatrix();{
			glColor3ub(10,100,100);
			glTranslatef(0,0,front_height);
			glPushMatrix();{
				drawRectangle(length,front_width,1);
				glColor3ub(100,100,100);
				glTranslatef(-17/2,0,0);
				glRotatef(90,0,1,0);
				gluCylinder(quadObj, 5, 5, 17, 30, 30); //gluCylinder(quadObj, base, top, height, slices, stacks)
			}glPopMatrix();
			
			glPushMatrix();{
				glTranslatef(-3.5,0,5);
				drawTupi(2.5,20,false);
				glTranslatef(7,0,0);
				drawTupi(2.5,20,false);
			
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-3.5,0,0);
				glRotatef(90,1,0,0);
				gluCylinder(quadObj, 5, 5, 5, 30, 30); //gluCylinder(quadObj, base, top, height, slices, stacks)
				
				glPushMatrix();{
					glTranslatef(0,0,5);
					gluDisk(quadObj, 0,5 , 30, 30);
				}glPopMatrix();
				
				glTranslatef(7,0,0);
				gluCylinder(quadObj, 5, 5, 5, 30, 30);
				glPushMatrix();{
					glTranslatef(0,0,5);
					gluDisk(quadObj, 0,5 , 30, 30);
				}glPopMatrix();
			
			}glPopMatrix();
			
		}glPopMatrix();
		
		//left
		glPushMatrix();{
			
			glColor3ub(0,00,00);
			glTranslatef(-(length-left_cube_length)/2,0,front_height);
			drawRectangle(left_cube_length,front_width,3.21);
			glColor3ub(0,00,0);
			glTranslatef(0,0,3.21);
			drawRectangle(left_cube_length-1,front_width,5.5);
			
			glTranslatef(0,0,5.5);
			drawRectangle(left_cube_length-2,front_width,5.5);
			
			//top one
			//glColor3ub(122,118,114);
			glTranslatef(0,0,5.5);
			drawRectangle(left_cube_length-3,front_width,5.5);
				
			glTranslatef(0,front_width/2,5.5);
			glRotatef(90,1,0,0);
			gluCylinder(quadObj, (left_cube_length-3)/2, (left_cube_length-3)/2, front_width, 30, 30); //gluCylinder(quadObj, base, top, height, slices, stacks)
			glPushMatrix();{
				//glColor3ub(0,0,0);
				glTranslatef(0,0,front_width);
				gluDisk(quadObj, 0,(left_cube_length-3)/2 , 30, 30); //gluDisk(quadObj, innerRadius, outerRadius, slices, loops)
			}glPopMatrix();
			//top one end
			
		}glPopMatrix();
		//right
		glPushMatrix();{
			
			glColor3ub(0,00,00);
			glTranslatef((length-left_cube_length)/2,0,front_height);
			drawRectangle(left_cube_length,front_width,3.21);
			glColor3ub(0,00,0);
			glTranslatef(0,0,3.21);
			drawRectangle(left_cube_length-1,front_width,5.5);
			
			glTranslatef(0,0,5.5);
			drawRectangle(left_cube_length-2,front_width,5.5);

			//top one
			//glColor3ub(122,118,114);
			glTranslatef(0,0,5.5);
			drawRectangle(left_cube_length-3,front_width,5.5);
				
			glTranslatef(0,front_width/2,5.5);
			glRotatef(90,1,0,0);
			gluCylinder(quadObj, (left_cube_length-3)/2, (left_cube_length-3)/2, front_width, 30, 30); //gluCylinder(quadObj, base, top, height, slices, stacks)
			glPushMatrix();{
				//glColor3ub(0,0,0);
				glTranslatef(0,0,front_width);
				gluDisk(quadObj, 0,(left_cube_length-3)/2 , 30, 30); //gluDisk(quadObj, innerRadius, outerRadius, slices, loops)
			}glPopMatrix();
			//top one end
			
		}glPopMatrix();
	}glPopMatrix();
	

	float bakano_rec_length = 6.5;
	float bakano_rec_width = 12;
	float bakano_rec_fheight = 11;
	float bakano_rec_bheight = 15;
	//right
	glPushMatrix();{
		glColor3ub(180,50,50);
		glTranslatef((length-bakano_rec_length)/2,-(width/2+front_width+bakano_rec_width/2),0);
		drawHelanoRectangle(bakano_rec_length,bakano_rec_width,bakano_rec_fheight,bakano_rec_bheight,Color(120,120,120));
	}glPopMatrix();
	//left
	glPushMatrix();{
		glColor3ub(180,50,50);
		glTranslatef(-(length-bakano_rec_length)/2,-(width/2+front_width+bakano_rec_width/2),0);
		drawHelanoRectangle(bakano_rec_length,bakano_rec_width,bakano_rec_fheight,bakano_rec_bheight,Color(120,120,120));
	}glPopMatrix();
	//center one

	glPushMatrix();{
		glColor3ub(180,50,50);
		glTranslatef(0,-(width/2+front_width+bakano_rec_width*2/3),0);
		drawHelanoRectangle(bakano_rec_length*2/3,bakano_rec_width*2/3,bakano_rec_fheight,bakano_rec_bheight,Color(120,120,120));
	}glPopMatrix();
	



}

void drawGumbujwalaMotaPillar(float radius,float height,bool isSolid, struct Color color) {
	glPushMatrix();{
		gluCylinder(quadObj, radius, radius, height, 30, 30); //gluCylinder(quadObj, base, top, height, slices, stacks)
		glTranslatef(0,0,height);
		glColor3ub(color.r,color.g,color.b);
		drawTupi(radius,30,isSolid);
	}glPopMatrix();
}

void drawbackside1() {
	float radius = 22/2;
	float height = 37;
	
	glColor3ub(CLR_WHITE);
	
	glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, tex_redwall);
		gluQuadricNormals(quadObj,GLU_SMOOTH);
		gluQuadricTexture(quadObj, GLU_TRUE);
		gluCylinder(quadObj, radius, radius, height-3, 30, 30);
	glDisable(GL_TEXTURE_2D);
	
	glColor3ub(CLR_WHITE);
	glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, tex_pinkwall);
		gluQuadricNormals(quadObj,GLU_SMOOTH);
		gluQuadricTexture(quadObj, GLU_TRUE);
		gluCylinder(quadObj, radius-1, radius-1, height, 30, 30);
	glDisable(GL_TEXTURE_2D);
	//gluCylinder(quadObj, radius-1, radius-1, height, 30, 30);
	//circulaar janala
	glPushMatrix();{
		glTranslatef(0,0,height-4);
		drawCircularWindow(radius-1,10,1.5,3,0.3,Color(CLR_BLACK),Color(CLR_LIGHT_GRAY));
	}glPopMatrix();

	glColor3ub(CLR_DARKER_GRAY);
	glTranslatef(0,0,height);
	glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, tex_tupi);
		gluQuadricNormals(quadObj,GLU_SMOOTH);
		gluQuadricTexture(quadObj, GLU_TRUE);
		gluCylinder(quadObj, radius, 5, 4, 30, 30);
	glDisable(GL_TEXTURE_2D);
	//gluCylinder(quadObj, radius, 5, 4, 30, 30);//conic 
	
	drawTupi(radius-4,30,true);	
}

void drawbackside2() {
	float radius = 10/2;
	float height = 28;
	glColor3ub(CLR_BRICK);
	drawGumbujwalaMotaPillar(radius,height,true,Color(CLR_DARK_GRAY));	
	//circulaar janala
	glPushMatrix();{
		glTranslatef(0,0,height-4);
		drawCircularWindow(radius,8,1.5,3,0.3,Color(CLR_BLACK),Color(CLR_LIGHT_GRAY));
	}glPopMatrix();
}

void drawbackside3() {
	float length = 10;
	float width = 6;
	float bheight = 23;
	float fheight = bheight - 4;
	glColor3ub(CLR_BRICK);
	drawHelanoRectangle(length,width,fheight,bheight,Color(CLR_DARKER_GRAY));
	//janala gula
	glPushMatrix();{
		glTranslatef(length/2,0,bheight-9);
		glRotatef(90,0,0,1);
		
		drawWindow(2,3,0.3,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
		glTranslatef(0,0,-7);
		drawWindow(3,4.5,0.3,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
	}glPopMatrix();
	
}

void drawbackside4() {
	float radius = 18/2;
	float height = 21;

	glPushMatrix();{

		//glTranslatef(0,0,legheight+1);
		glColor3ub(CLR_WHITE);
		glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, tex_brick);
			gluQuadricNormals(quadObj,GLU_SMOOTH);
			gluQuadricTexture(quadObj, GLU_TRUE);
			gluCylinder(quadObj, radius, radius, height, 6, 40);
		glDisable(GL_TEXTURE_2D);
		
		glColor3ub(CLR_WHITE);
	glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, tex_redwall);
		gluQuadricNormals(quadObj,GLU_SMOOTH);
		gluQuadricTexture(quadObj, GLU_TRUE);
		gluCylinder(quadObj, radius-2, radius-2, height+3.5, 30, 40);
	glDisable(GL_TEXTURE_2D);
		//gluCylinder(quadObj, radius-2, radius-2, height+3.5, 30, 40);
		//circulaar janala
		glPushMatrix();{
			glTranslatef(0,0,height);
			drawCircularWindow(radius-2,15,1,2,0.3,Color(CLR_BLACK),Color(CLR_DARK_GRAY));
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,0,height);
			glColor3ub(CLR_BLACK);
			gluDisk(quadObj,0,radius,6,30);
		}glPopMatrix();
		//
		
		glTranslatef(0,0,height+3.5);
		glColor3ub(CLR_DARKER_GRAY);
		drawTupi(radius-2,10,true);
		
	}glPopMatrix();
	//janala gula
	glPushMatrix();{
		glTranslatef(0,-radius,height-9);
		drawWindow(3,4.5,0.5,Color(CLR_DARKER_GRAY),Color(CLR_DARK_GRAY));
		glTranslatef(0,0,-7);
		drawWindow(3,4.5,0.5,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
	}glPopMatrix();
	
}

void drawbackside5() {
	float height = 15;
	float width = 14;
	float length = 14;

	glColor3ub(CLR_BRICK2);
	drawRectangleWithTexture(length,width,height,tex_brick);
	//janala gula
	glPushMatrix();{
		glTranslatef(0,-width/2,height+1.5);
		drawWindow(1.7,4,0.3,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
		glTranslatef(4,0,0);
		drawWindow(1.2,3,0.2,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
		glTranslatef(-8,0,0);
		drawWindow(1.2,3,0.2,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
		glTranslatef(4,-0.2,-10);
		drawWindow2(2.5,5,0.4,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
		glTranslatef(-4,0,0);
		drawWindow2(2.5,5,0.4,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
	}glPopMatrix();
	
	glPushMatrix();{
		glRotatef(90,0,0,1);
		glPushMatrix();{
			glTranslatef(0,-length/2,height+1.5);
			drawWindow(1.7,4,0.3,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
			glTranslatef(4,0,0);
			drawWindow(1.2,3,0.2,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
			glTranslatef(-8,0,0);
			drawWindow(1.2,3,0.2,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
			glTranslatef(4,0,-10);
			drawWindow2(2.5,5,0.4,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
		}glPopMatrix();
	}glPopMatrix();
	
	//janala sesh
	glPushMatrix();{
		glColor3ub(CLR_DARK_GRAY);
		glTranslatef(-length/2,0,height);
		glRotatef(90,0,1,0);
		gluCylinder(quadObj, width/2, width/2, length, 30, 30);
		glPushMatrix();{
			glColor3ub(CLR_BRICK);
			glTranslatef(0,0,1);
			gluDisk(quadObj, 0,width/2 , 30, 30);
			glTranslatef(0,0,length-1.5);
			gluDisk(quadObj, 0,width/2 , 30, 30);
		}glPopMatrix();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3ub(CLR_DARKER_GRAY);
		
		glTranslatef(0,width/2,height);
		glRotatef(90,1,0,0);
		gluCylinder(quadObj, length/2, length/2, width, 30, 30); 
		glPushMatrix();{
			glColor3ub(CLR_BRICK);
			glTranslatef(0,0,1);
			gluDisk(quadObj, 0,width/2 , 30, 30);
			glTranslatef(0,0,width-1.5);
			gluDisk(quadObj, 0,width/2 , 30, 30);
		}glPopMatrix();
	}glPopMatrix();
	glPushMatrix();{
		glColor3ub(120,120,120);
		glTranslatef(0,0,height+width/2-2);
		drawTupi(5,10,false);
	}glPopMatrix();
}

void drawbackside6(){
	float length = 56;
	float width = 10;
	
	//dane mota rectangle
	glPushMatrix();{
		glTranslatef(8,-15/2,0);
		glColor3ub(CLR_WHITE);
		drawHelanoRectangleTex(9,15,15,18,Color(CLR_DARKER_GRAY),tex_graywall);
	}glPopMatrix();
	//ekdom daner ta
	glPushMatrix();{
		glTranslatef(length/2,-11/2,0);
		glColor3ub(CLR_WHITE);
		drawHelanoRectangleTex(11,11,12,14,Color(CLR_DARKER_GRAY),tex_redwall);
	}glPopMatrix();
	//bamer akabaka rec
	glPushMatrix();{
		//daner ta
		glTranslatef(-5,0,0);
		glColor3ub(CLR_DARK_GRAY);
		drawcurcularRectangleDouble(3.55,width,0,12,13);
		//bamer ta
		glTranslatef(-8,0,0);
		glColor3ub(CLR_DARK_GRAY);
		drawcurcularRectangleDouble(2.55,width,0,12,13);
	}glPopMatrix();
	//ekdom bamer ta
	glPushMatrix();{
		glTranslatef(-(length+6.8)/2,-12/2,0);
		glColor3ub(CLR_WHITE);
		drawHelanoRectangleTex(6.8,15,12,14,Color(CLR_DARKER_GRAY),tex_graywall);
	}glPopMatrix();
	//nicher lomba half wall ta
	glPushMatrix();{
		glTranslatef(0,-(width+1),0);
		glColor3ub(CLR_WHITE);
		drawHelanoRectangleTex(length,2,4.52,4.52,Color(CLR_DARKER_GRAY),tex_graywall);
	}glPopMatrix();
	//daner lomba pillar ta(ninu)
	glPushMatrix();{
		glTranslatef((length+6.4)/2,-(width+6.4/2),0);
		//glColor3ub(CLR_DARK_GRAY);
		drawFrontCornerPiller(Color(CLR_LIGHTER_GRAY));
	}glPopMatrix();
	//bamer lomba pilar
	glPushMatrix();{
		glTranslatef(-(length+6.4+5)/2,-(width+6.4/2),0);
		//glColor3ub(CLR_DARK_GRAY);
		drawFrontCornerPiller2(Color(CLR_BRICK3));
	}glPopMatrix();
}

void drawBackSide() {
	//1
	glPushMatrix();{
		//glColor3ub(10,100,100);
		glTranslatef(0,0,0);
		drawbackside1();
	}glPopMatrix();
	//2
	glPushMatrix();{
		//glColor3ub(100,10,100);
		glTranslatef(10,-2,0);
		drawbackside2();
	}glPopMatrix();
	glPushMatrix();{
		//glColor3ub(100,10,100);
		glTranslatef(-10,-2,0);
		drawbackside2();
	}glPopMatrix();

	//3
	glPushMatrix();{
		//glColor3ub(100,100,10);
		glTranslatef(11,-8,0);
		glRotatef(90,0,0,1);
		drawbackside3();
	}glPopMatrix();
	glPushMatrix();{
		//glColor3ub(100,100,10);
		glTranslatef(-11,-8,0);
		glRotatef(-90,0,0,1);
		drawbackside3();
	}glPopMatrix();
	//4
	glPushMatrix();{
		//glColor3ub(80,100,100);
		glTranslatef(0,-11,0);
		drawbackside4();
	}glPopMatrix();
	//5
	glPushMatrix();{
		//glColor3ub(10,100,10);
		glTranslatef(21,-7,0);
		drawbackside5();
	}glPopMatrix();
	glPushMatrix();{
		//glColor3ub(10,100,10);
		glTranslatef(-21,-7,0);
		drawbackside5();
	}glPopMatrix();
	//6
	glPushMatrix();{
		glTranslatef(0,-14,0);
		drawbackside6();
	}glPopMatrix();


}

void drawfrontside1() {
	float radius = 22/2;
	float height = 32+4;
	
	glColor3ub(CLR_WHITE);
		glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, tex_redwall);
			gluQuadricNormals(quadObj,GLU_SMOOTH);
			gluQuadricTexture(quadObj, GLU_TRUE);
			gluCylinder(quadObj, radius, radius, height-3, 30, 30);
		glDisable(GL_TEXTURE_2D);
	//gluCylinder(quadObj, radius, radius, height-3, 30, 30);
		glColor3ub(CLR_WHITE);
		glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, tex_pinkwall);
			gluQuadricNormals(quadObj,GLU_SMOOTH);
			gluQuadricTexture(quadObj, GLU_TRUE);
			gluCylinder(quadObj, radius-1, radius-1, height, 30, 30);
		glDisable(GL_TEXTURE_2D);
	//gluCylinder(quadObj, radius-1, radius-1, height, 30, 30);
	
	//circulaar janala
	glPushMatrix();{
		glTranslatef(0,0,height-3);
		drawCircularWindow(radius-1,10,1.5,2.5,0.3,Color(CLR_BLACK),Color(CLR_LIGHT_GRAY));
		glTranslatef(0,0,-1.5);
		drawCircularBlock(radius,10,1.5,0.2,1.5,Color(CLR_BLACK));
	}glPopMatrix();

	glColor3ub(CLR_DARK_GRAY);
	glTranslatef(0,0,height);
	glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, tex_tupi);
		gluQuadricNormals(quadObj,GLU_SMOOTH);
		gluQuadricTexture(quadObj, GLU_TRUE);
		gluCylinder(quadObj, radius, 5, 4, 30, 30);
	glDisable(GL_TEXTURE_2D);
	//gluCylinder(quadObj, radius, 5, 4, 30, 30);
	
	
	drawTupi(radius-4,30,true);	
}

void drawfrontside2() {
	float radius = 16/2;
	float height = 23;

	drawGumbujwalaMotaPillar(radius,height,true,Color(CLR_LIGHT_GRAY));	
}

void drawfrontside3() {
	float length = 30;
	float width = 12;
	float height = 25;

	drawRectangle(length,width,height-1);
	glColor3ub(CLR_DARKER_GRAY);
	glTranslatef(0,0,height-1);
	drawRectangle(length,width,1);
}

void drawfrontside4() {
	float length = 26;
	float width = 4;
	float height = 29;
	
	glPushMatrix();{
		glColor3ub(CLR_WHITE);
		glTranslatef(0,-(width/4+width/2),0);
		drawHelanoRectangleTex(26,width/2,height,height+2,Color(CLR_DARKER_GRAY),tex_brick);
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3ub(CLR_WHITE);
		glTranslatef(0,-width/4,0);
		glRotatef(180,0,0,1);
		drawHelanoRectangleTex(26,width/2,height,height+2,Color(CLR_DARKER_GRAY),tex_brick);
	}glPopMatrix();
}

void drawfrontside5() {
	float radius = 4/2;
	float height = 4;
	glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, tex_brick);
		gluQuadricNormals(quadObj,GLU_SMOOTH);
		gluQuadricTexture(quadObj, GLU_TRUE);
		gluCylinder(quadObj, radius, radius, height, 30, 30); 
	glDisable(GL_TEXTURE_2D);
	//gluCylinder(quadObj, radius, radius, height, 30, 30); 
	glTranslatef(0,0,height);
	glColor3ub(CLR_WHITE);
	drawTupi(radius,8,false);
}

void drawfrontside6() {
	float height = 15;
	float width = 14;
	float length = 14;

	glColor3ub(CLR_BRICK2);
	drawRectangleWithTexture(length,width,height,tex_brick);
		//janala gula
	glPushMatrix();{
		glTranslatef(0,-width/2,height+1.5);
		drawWindow(1.7,4,0.3,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
		glTranslatef(4,0,0);
		drawWindow(1.2,3,0.2,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
		glTranslatef(-8,0,0);
		drawWindow(1.2,3,0.2,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
		glTranslatef(4,0,-10);
		drawWindow(2.5,5,0.4,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
	}glPopMatrix();
	
	glPushMatrix();{
		glRotatef(90,0,0,1);
		glPushMatrix();{
			glTranslatef(0,-length/2,height+1.5);
			drawWindow(1.7,4,0.3,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
			glTranslatef(4,0,0);
			drawWindow(1.2,3,0.2,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
			glTranslatef(-8,0,0);
			drawWindow(1.2,3,0.2,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
			glTranslatef(4,0,-10);
			drawWindow(2.5,5,0.4,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
		}glPopMatrix();
	}glPopMatrix();
	
	//janala sesh
	
	glPushMatrix();{
		glColor3ub(CLR_DARK_GRAY);
		glTranslatef(-length/2,0,height);
		glRotatef(90,0,1,0);
		gluCylinder(quadObj, width/2, width/2, length, 30, 30);
		glPushMatrix();{
			glColor3ub(CLR_BRICK);
			glTranslatef(0,0,1);
			gluDisk(quadObj, 0,width/2 , 30, 30);
			glTranslatef(0,0,length-1.5);
			gluDisk(quadObj, 0,width/2 , 30, 30);
		}glPopMatrix();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3ub(CLR_DARKER_GRAY);
		
		glTranslatef(0,width/2,height);
		glRotatef(90,1,0,0);
		gluCylinder(quadObj, length/2, length/2, width, 30, 30);
		glPushMatrix();{
			glColor3ub(CLR_BRICK);
			glTranslatef(0,0,1);
			gluDisk(quadObj, 0,width/2 , 30, 30);
			glTranslatef(0,0,width-1.5);
			gluDisk(quadObj, 0,width/2 , 30, 30);
		}glPopMatrix();
	}glPopMatrix();
	glPushMatrix();{
		glColor3ub(120,120,120);
		glTranslatef(0,0,height+width/2-2);
		drawTupi(5,10,false);
	}glPopMatrix();

}

void drawfrontside7() {
	float radius = 13;
	float height = 6;

	double equ[4];

	equ[0] = 0;	//0.x
	equ[1] = 0;	//0.y
	equ[2] = 1;//-1.z
	equ[3] = 0;//30

	glClipPlane(GL_CLIP_PLANE0,equ);

	//now we enable the clip plane

	glEnable(GL_CLIP_PLANE0);{
		glPushMatrix();{
		
			glRotatef(90,1,0,0);
			glColor3ub(CLR_WHITE1);
			
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, tex_tupi);
			gluQuadricNormals(quadObj,GLU_SMOOTH);
			gluQuadricTexture(quadObj, GLU_TRUE);

			gluCylinder(quadObj, radius/2, radius/2, height, 30, 30); 
			gluCylinder(quadObj, radius/2-1, radius/2-1, height, 30, 30);
			glTranslatef(0,0,height);
			glColor3ub(CLR_DARKER2_GRAY);
			gluDisk(quadObj,radius/2-1,radius/2,30,30);
			glTranslatef(0,0,-0.2);
			glColor3ub(CLR_BLACK);
			gluDisk(quadObj,0,radius/2,30,30);
			
			glDisable(GL_TEXTURE_2D);

			
		
		}glPopMatrix();

	}glDisable(GL_CLIP_PLANE0);

}

void drawfrontside8() {
	float length = 26;
	float front_length = 56;
	float width = 5.7;
	float height = 18;
	float ext = 4;
	
	glPushMatrix();{
		glTranslatef(0,-width/2,0);
		glColor3ub(CLR_BRICK2);
		drawHelanoRectangle(length,width,height,height+ext,Color(CLR_DARK_GRAY));
		glTranslatef(0,-width,0);
		glColor3ub(CLR_WHITE);
		drawHelanoRectangleTex(front_length,width,height-ext,height,Color(CLR_DARK_GRAY),tex_redwall);
	}glPopMatrix();
	//side er 2 ta rectangle
	float side_length = 24;
	glPushMatrix();{
		glTranslatef((length+side_length)/2,-width/2,0);
		glColor3ub(CLR_BRICK2);
		drawHelanoRectangleTex(side_length,width,height,height,Color(CLR_DARKER2_GRAY),tex_redwall);
		
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-(length+side_length)/2,-width/2,0);
		glColor3ub(CLR_BRICK2);
		drawHelanoRectangleTex(side_length,width,height,height,Color(CLR_DARKER2_GRAY),tex_redwall);
		
	}glPopMatrix();
	//side er gular upore aro 2 ta rectangle
	float side_width = 4;
	glPushMatrix();{
		glTranslatef((length+side_length)/2,side_width/2,0);
		glColor3ub(CLR_BRICK2);
		drawHelanoRectangleTex(side_length,side_width,height-ext,height-ext,Color(CLR_DARKER_GRAY),tex_redwall);
		
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-(length+side_length)/2,side_width/2,0);
		glColor3ub(CLR_BRICK2);
		drawHelanoRectangleTex(side_length,side_width,height-ext,height-ext, Color(CLR_DARKER_GRAY),tex_redwall);
		
	}glPopMatrix();
	//samner janala gula
	glPushMatrix();{
		glTranslatef(0,-(width*2)-0.2,5);
		drawWindow2(2.5,5.5,0.4,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
		
		glPushMatrix();{
			glTranslatef(7.63,-0.2,0);
			drawWindow2(2.5,5.5,0.4,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
			glTranslatef(-7.63*2,0,0);
			drawWindow2(2.5,5.5,0.4,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
		}glPopMatrix();

		glPushMatrix();{
			glTranslatef(17.63,-0.2,0);
			drawWindow2(2.5,5.5,0.4,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
			glTranslatef(-17.63*2,0,0);
			drawWindow2(2.5,5.5,0.4,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
		}glPopMatrix();

		glPushMatrix();{
			glTranslatef(22.63,-0.2,0);
			drawWindow2(2.5,5.5,0.4,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
			glTranslatef(-22.63*2,0,0);
			drawWindow2(2.5,5.5,0.4,Color(CLR_WHITE),Color(CLR_DARK_GRAY));
		}glPopMatrix();
		

	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-8.25,-(width*2)-8,-3);
		drawCircularWall();
		
	}glPopMatrix();
	//samner bakano rectangle gula
	glPushMatrix();{
		glTranslatef(-4.25,-(width*2),-3);
		drawcurcularRectangle(3.25,12,3,14,14);
		
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(4.25,-(width*2),-3);
		drawcurcularRectangle(3.25,12,3,14,14);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-11.25,-(width*2),-3);
		drawcurcularRectangle(3.25,12,3,12,14);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(11.25,-(width*2),-3);
		drawcurcularRectangle(3.25,12,3,12,14);
	}glPopMatrix();
	//left er rectangle ta
	glPushMatrix();{
		glTranslatef(-(front_length-12.65-1)/2,-(width*2+7/2),0);
		glColor3ub(CLR_BRICK3);
		drawHelanoRectangleTex(12.65,7,6,6,Color(CLR_DARKER_GRAY),tex_redwall);
	}glPopMatrix();
	//etar bamer 2 ta rectangle
	glPushMatrix();{
		glTranslatef(-(front_length-2)/2,-(width*2+8.5/2),0);
		glColor3ub(CLR_BRICK2);
		drawHelanoRectangleTex(2,8.5,13.5,13.5,Color(CLR_BRICK3),tex_pinkwall);
		glTranslatef(0,-(8.5+6.5)/2,0);
		glColor3ub(CLR_BRICK2);
		drawHelanoRectangleTex(2,6.5,6.8,6.8,Color(CLR_BRICK3),tex_pinkwall);
		glTranslatef(-10,5,0);
		drawOuterGumbujj2(14,6,6,false);
	}glPopMatrix();
	//ekdom daner motasota rectangle ta
	glPushMatrix();{
		glTranslatef((front_length/2+2.5),-(width*2+14.40/2),0);
		glColor3ub(CLR_BRICK);
		drawHelanoRectangleTex(10.5,14.4,13.5,13.5,Color(CLR_DARKER_GRAY),tex_pinkwall);
		
	}glPopMatrix();

	//samner lomba pillar 2 ta
	glPushMatrix();{
		glTranslatef(-(front_length+6.4)/2,-(width*2-6.4/2),0);
		drawFrontCornerPiller(Color(CLR_LIGHTER_GRAY));
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef((front_length+6.4)/2,-(width*2-6.4/2),0);
		drawFrontCornerPiller(Color(CLR_LIGHTER_GRAY));
	}glPopMatrix();
}

void drawfrontside() {
	
	//1
	glPushMatrix();{
		glColor3ub(CLR_BRICK);
		glTranslatef(0,0,0);
		drawfrontside1();

	}glPopMatrix();
	//2
	glPushMatrix();{
		glColor3ub(CLR_BRICK);
		glTranslatef(6,-4,0);
		drawfrontside2();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3ub(CLR_BRICK);
		glTranslatef(-6,-4,0);
		drawfrontside2();
	}glPopMatrix();
	//3
	glPushMatrix();{
		glColor3ub(CLR_BRICK2);
		glTranslatef(0,-6,0);
		drawfrontside3();
	}glPopMatrix();
	//4
	glPushMatrix();{
		glColor3ub(CLR_BRICK);
		glTranslatef(0,-12,0);
		drawfrontside4();
	}glPopMatrix();
	//5
	glPushMatrix();{
		glColor3ub(CLR_BRICK);
		glTranslatef(10,-14,29);
		drawfrontside5();
	}glPopMatrix();
		glPushMatrix();{
		glColor3ub(CLR_BRICK);
		glTranslatef(-10,-14,29);
		drawfrontside5();
	}glPopMatrix();

	//6
	glPushMatrix();{
		glColor3ub(10,100,10);
		glTranslatef(21,-7,0);
		drawfrontside6();
	}glPopMatrix();
	glPushMatrix();{
		glColor3ub(10,100,10);
		glTranslatef(-21,-7,0);
		drawfrontside6();
	}glPopMatrix();

	//7
	glPushMatrix();{
		glColor3ub(CLR_BLACK);
		glTranslatef(0,-11,25);
		drawfrontside7();
	}glPopMatrix();
	//8
	glPushMatrix();{
		//glColor3ub(120,100,120);
		glTranslatef(0,-15,0);
		drawfrontside8();
	}glPopMatrix();
	
}

void drawHalfTorus(float radius) {
	
	float height = 2;
	double equ[4];

	equ[0] = 0;	//0.x
	equ[1] = 0;	//0.y
	equ[2] = -1;//-1.z
	equ[3] = 0;//30

	glClipPlane(GL_CLIP_PLANE0,equ);

	//now we enable the clip plane

	glEnable(GL_CLIP_PLANE0);{
		glPushMatrix();{
		
			glTranslatef(0,(height)/2,0);
			glRotatef(90,1,0,0);
			gluCylinder(quadObj, radius/2, radius/2, height, 30, 30); //gluCylinder(quadObj, base, top, height, slices, stacks)
		
		
		}glPopMatrix();

	}glDisable(GL_CLIP_PLANE0);
}



void drawHagia() {
	
	float length = 30;
	//center
	glPushMatrix();{
		drawcenterObject();
	}glPopMatrix();
	//right
	glPushMatrix();{
		glTranslatef(length/2,0,0);
		glRotatef(90,0,0,1);
		drawright();
	}glPopMatrix();
	//left
	glPushMatrix();{
		glTranslatef(-length/2,0,0);
		glRotatef(-90,0,0,1);
		drawleft();
	}glPopMatrix();
	//front
	glPushMatrix();{
		glTranslatef(0,-length/2,0);
		drawfrontside();
	}glPopMatrix();
	//back
	glPushMatrix();{
		glTranslatef(0,length/2,0);
		glRotatef(180,0,0,1);
		drawBackSide();
	}glPopMatrix();
}

void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(CLR_BLACK, 0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
	/ set-up camera here
	********************/
	//load the correct matrix -- MODEL-VIEW matrix
	glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera is looking?
	//3. Which direction is the camera's UP direction?
	
	if(center_camera == 1) {
		gluLookAt(0,0,130,	100*sin(cameraAngle),100*cos(cameraAngle),0,	0,0,1);
	}
	
	else if(rotate_camera ==1) {
		gluLookAt(100*sin(cameraAngle),100*cos(cameraAngle),cameraFromZ,  0,0,0,	0,0,1);
	}
	
	else {
		//gluLookAt(cameraFromX,cameraFromY,cameraFromZ,	0,0,0,	0,0,1);
		gluLookAt(camera.position.x, camera.position.y, camera.position.z, camera.lookat.x, camera.lookat.y, camera.lookat.z,
			camera.updirection.x, camera.updirection.y, camera.updirection.z);

	}
	
	//gluLookAt(0,0,100,	100*sin(cameraAngle),150*cos(cameraAngle),0,	0,0,1);
	
	//again select MODEL-VIEW
	glMatrixMode(GL_MODELVIEW);


	/****************************
	/ Add your objects from here
	****************************/

	//light objects

	if(isLighting == 1) {
		glEnable(GL_LIGHTING);
	}
	else {
		glDisable(GL_LIGHTING);
	}
	
	if(isDayMode == 1) {
		GLfloat lmodel_ambient[] = { 1, 1, .8, 1.0 }; //color of the global ambient light
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
	}
	else {
		GLfloat lmodel_ambient[] = { .1, .1, .1, 1.0 }; //color of the global ambient light
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
	}

	//moving light source
	 GLfloat position[] = {-100.0*cos(lightAngle),-100.0*sin(lightAngle),30,1.0}; 
	 if(isWhiteLight == 1) {
		GLfloat diffuseColor[] = {1, 1, 1, 1.0};//white color
		glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuseColor);

	 }
	 else {
		GLfloat diffuseColor[] = {0, 0, 1, 1.0};//blue color
		GLfloat spot_direction[] = {-1, -1, 0};
		
		//glLightf(GL_LIGHT1,GL_SPOT_CUTOFF,45.0);
		//glLightf(GL_LIGHT1,GL_SPOT_EXPONENT ,5.0);
		//glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, spot_direction);
		
		glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuseColor);
		
		
	 }
     glLightfv(GL_LIGHT1, GL_POSITION, position);




	 //spot
	 if(spot == 1) {
			glDisable(GL_LIGHT1);
			glEnable(GL_LIGHT0);

			GLfloat spotLightPosition[] = {0, 2000, 500, 1};
			GLfloat cutOffAngle[] = {45.0};
			GLfloat fallOfExponent[] = {0.3};
			glLightfv(GL_LIGHT0, GL_POSITION, spotLightPosition); 
			glLightfv(GL_LIGHT0, GL_SPOT_CUTOFF, cutOffAngle);
			GLfloat spotDirection[] = {0, 1, -1};
			glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, spotDirection);
			glLightfv(GL_LIGHT0, GL_SPOT_EXPONENT, fallOfExponent);

			// draw spot light
			glPushMatrix();
			glColor3ub(0,0,1);
			glTranslatef (spotLightPosition[0], spotLightPosition[1], spotLightPosition[2]);
			glutSolidSphere(150, 36, 36);
			glPopMatrix();
	 }
	
	  // representative object of the light source 
 
	 //changing color of light source
	 GLfloat unset[]={0,0,0,1};
	 if(isWhiteLight == 1) {
	   GLfloat light_emission[] = {1.0, 1.0, 1.0, 1.0};
	   glMaterialfv(GL_FRONT, GL_EMISSION, light_emission);
	   glMaterialfv(GL_FRONT, GL_SPECULAR, unset);
	 } else {
		GLfloat light_emission[] = {0.0, 0.0, 1.0, 1.0};
	   glMaterialfv(GL_FRONT, GL_EMISSION, light_emission);
	   glMaterialfv(GL_FRONT, GL_SPECULAR, unset);
	 }
	   
	   glPushMatrix();
		   glTranslatef (-100.0*cos(lightAngle),-100.0*sin(lightAngle),30);
		   glutSolidSphere(4, 36, 36);
	   glPopMatrix();
	   

	 //object being illuminated
	   GLfloat mat_ambient[] = { 0.792, 0.392, 0.333, 1.0 };
	   GLfloat mat_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
	   GLfloat mat_specular[] = { 0.5, 0.5, 0.5, 1.0 }; //equal to the light source
	   GLfloat low_shininess[] = { 5.0 };
	   GLfloat mid_shininess[] = { 50.0 };
	   GLfloat high_shininess[] = { 100.0 };
  
	   glPushMatrix();
		   //glTranslatef (0, 0.0, 0.0);
		   //glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
		   //glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
		   //glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
		   //glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);
		   glMaterialfv(GL_FRONT, GL_EMISSION, unset);
		   drawHagia();
	   glPopMatrix();

	glPushMatrix();{

		//drawCircularWall();
		//drawWindow(5,10,0.5,Color(CLR_BRICK),Color(CLR_DARKER_GRAY));
		//drawRectangle(20,20,20);
		//glColor3ub(70,180,90);
		//drawTupi(30, 20,false);
		//drawOuterGumbuj();
		//drawMiddleStructure();
		//glColor3ub(CLR_WHITE1);
		//drawHelanoRectangleTex(10,20,10,20,Color(CLR_WHITE),tex_graywall);
		//drawBackSide();
		//drawbackside6();
		//drawcenterObject();
		//drawleft();
		//minar_top();
		//drawOuterGumbujj(30,10,6,false);
		//drawHagia();
		//drawHalfTorus(5);
		//drawFrontCornerPiller2(Color(CLR_BRICK3));
		//drawtupirmatha();
		//drawCircularTriangle(6,9,8,6,15);
		//drawcurcularRectangle(5,25,0,20,20);
		//drawCircularWindow(10,15,2,Color(CLR_BRICK),Color(CLR_LIGHTER_GRAY));
		//drawCircularWall(15);
		//glColor3ub(CLR_WHITE1);
		//drawRectangleWithTexture(20,10,30,tex_graywall);
		//drawRectangle(20,10,30);
		//drawCircularBlock(30,20,4,4);
		
	}glPopMatrix();

	//floor
	glPushMatrix();{
		glColor3ub(30,30,30);
		//drawRectangle(300,300,1);
	}glPopMatrix();


	//some gridlines along the field
	int i;

	glColor3f(0.5, 0.5, 0.5);	//grey
	glBegin(GL_LINES);{
		for(i=-10;i<=10;i++){

			if(i==0)
				continue;	//SKIP the MAIN axes

			//lines parallel to Y-axis
			//glVertex3f(i*10, -100, 0);
			//glVertex3f(i*10,  100, 0);

			//lines parallel to X-axis
			//glVertex3f(-100, i*10, 0);
			//glVertex3f( 100, i*10, 0);
		}
	}glEnd();
	
	// draw the two AXES
	glColor3f(1, 1, 1);	//100% white
	glBegin(GL_LINES);{
		//Y axis
		glColor3f(0,1,0);
		glVertex3f(0, -200, 0);	// intentionally extended to -150 to 150, no big deal
		glVertex3f(0,  200, 0);

		//X axis
		glColor3f(1,0,0);
		glVertex3f(-200, 0, 0);
		glVertex3f( 200, 0, 0);
	}glEnd();


	//ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glutSwapBuffers();
}

void animate(){
	//codes for any changes in Models, Camera
	if(center_camera == 1||rotate_camera == 1){
		if(camera_pause != 1)
			cameraAngle += 0.03;
	}

	lightAngle += 0.05;

	glutPostRedisplay();
}


void keyboardListener(unsigned char key, int x,int y){
	switch(key){

		case '1':
			camera.Roll(-5);
			break;

		case '2':
			camera.Roll(5);
			break;
			
		case '3':
			camera.Pitch(-5);
			break;
		case '4':
			camera.Pitch(5);
			break;
		case '5':
			camera.Yaw(-5);
			break;
			
		case '6':
			camera.Yaw(5);
			break;
			

		case 'c':	

			rotate_camera = 0;// turn off rotate camera
			if(center_camera == 1) 
				center_camera = 0;
			else
				center_camera = 1;
			break;
		case 'r':
			center_camera = 0;//turn off center camera
			if(rotate_camera == 1) {
				rotate_camera = 0;
				camera.reset();
			}
			else
				rotate_camera = 1;
			break;
		case 'p':
			
			if(camera_pause == 1) 
				camera_pause = 0;
			else
				camera_pause = 1;
			break;
		case 'x':
			
			camera.reset();
			initial_pos = (initial_pos+1)%4;
				
			break;
		case 'd':
			
			if(isDayMode == 1)
				isDayMode = 0;
			else
				isDayMode = 1;
				
			break;
		case 'w':
			
			if(isWhiteLight == 1)
				isWhiteLight = 0;
			else
				isWhiteLight = 1;
				
			break;
		case 'l':
			
			if(isLighting == 1)
				isLighting = 0;
			else
				isLighting = 1;
				
			break;
		case 's':
			
			if(spot == 1)
				spot = 0;
			else
				spot = 1;
				
			break;

		case 27:	//ESCAPE KEY -- simply exit
			exit(0);
			break;

		default:
			break;
	}
}
void resetCam() {
	
		center_camera = 0;
		rotate_camera = 0;
		camera_pause = 0;
}
void specialKeyListener(int key, int x,int y){
	switch(key){
		case GLUT_KEY_DOWN:		//down arrow key
			resetCam();
			camera.walk(5);
			break;
		case GLUT_KEY_UP:		// up arrow key
			resetCam();
			camera.walk(-5);
			break;

		case GLUT_KEY_RIGHT:
			resetCam();
			camera.straf(5);
			break;
		case GLUT_KEY_LEFT:
			resetCam();
			camera.straf(-5);
			break;

		case GLUT_KEY_PAGE_UP:
			resetCam();
			camera.fly(5);
			break;
		case GLUT_KEY_PAGE_DOWN:
			resetCam();
			camera.fly(-5);
			break;

		case GLUT_KEY_INSERT:
			break;

		case GLUT_KEY_HOME:
			break;
		case GLUT_KEY_END:
			break;

		default:
			break;
	}
}


void init(){
	//codes for initialization
	
	cameraAngle = 0;
	spot = 0;
	lightAngle = 0;
	isWhiteLight = 1;
	isDayMode = 1;
	isLighting = 0;

	initial_pos = 1;

	cameraFromX = 0;
	cameraFromY = -80;
	cameraFromZ = 50;

	cameraToX = 0;
	cameraToY = 0;
	cameraToZ = 0;

	rotateXY = 0;
	rotateYZ = 0;
	rotateZX = 0;

	center_camera = 0;
	rotate_camera = 0;
	camera_pause = 0;


	initTexture();
	
	//clear the screen
	glClearColor(CLR_BLACK, 0);
	
	//Lighting code
	//set the shading model
	glShadeModel(GL_SMOOTH);
	
	//Add global ambient light
	//GLfloat lmodel_ambient[] = { 1, 1, 0.8, 1.0 }; //color of the global ambient light
	//glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

    //effect of local light source: directed diffuse light
    //GLfloat diffuseDir[] = {0.5, 0.5, 0.5, 1.0}; //Color (0.5, 0.2, 0.2)
    //GLfloat lightDir[] = {1.0, 0.5, 0.5, 0.0}; //Coming from the direction (-1, 0.5, 0.5)
    //glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseDir);
    //glLightfv(GL_LIGHT0, GL_POSITION, lightDir);

	
	//effect of local light source: point diffuse light
   //GLfloat diffusePoint[] = {1, 1, 1, 1.0}; //Color (0.5, 0.5, 0.5)
   //GLfloat position[] = {-10.0, 50.0, 5.0, 1.0}; //Positioned at (-10, -10, 5);
   //glLightfv(GL_LIGHT1, GL_DIFFUSE, diffusePoint); //
   //glLightfv(GL_LIGHT1, GL_POSITION, position);

	//spot
	GLfloat spotLightPoint[] = {0.8, 0.8, 0.8, 1.0}; 
	glLightfv(GL_LIGHT0, GL_DIFFUSE, spotLightPoint);
   
    glEnable(GL_NORMALIZE); //Automatically normalize normals needed by the illumination model
    glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	//glEnable(GL_LIGHT0); //Enable light #0
	glEnable(GL_LIGHT1); //Enable light #1

   //end of lighting code

	/************************
	/ set-up projection here
	************************/
	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);
	
	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(70,	1,	0.1,	10000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
}

int main(int argc, char **argv){
	glutInit(&argc,argv);
	glutInitWindowSize(900, 600);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("My OpenGL Program");

	init();

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}



