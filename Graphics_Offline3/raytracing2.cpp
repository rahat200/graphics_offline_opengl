// raytracing2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "utils.h"

using namespace std;

ifstream fin;
string buffer;
int lineno;

int windowSize = MAX_SIZE;
Vector pixels[MAX_SIZE+5][MAX_SIZE+5];
Camera camera;

struct Ray{
    Vector O;
    Vector D;
};


struct Plane{
    Vector P;
    Vector N;
	Vector color;
    double xmax,xmin,ymax,ymin,zmax,zmin;
    double ambient,diffuse,speculer,reflect,specExp;
};

struct Coefficient{
    double ambient,diffuse,speculer,reflect,specExp;
};

struct Sphere{
    Vector C;
    double R;
    Vector color;
    double ambient,diffuse,speculer,reflect,specExp;
};

struct Cylinder{
	double xCenter,yCenter,radius,zMin,zMax;
    Vector color;
    double ambient,diffuse,speculer,reflect,specExp;
};

struct Triangle{
    Vector A,B,C;
    double R;
    Vector color;
    double ambient,diffuse,speculer,reflect,specExp,refrac_index;
};

struct Intersection{
    double t;
	string obj_type;
    int obj_num;
};

int maxDepth;
int initialDepth = 1;

vector<Plane>PlaneList;
vector<Sphere>SphereList;
vector<Triangle>TriangleList;
vector<Cylinder>CylinderList;

vector<Vector>lightSources;


double calculate_t_triangle(Ray ray,Triangle triangle){
    
	Vector BA = triangle.A-triangle.B;
	Vector BC =  triangle.C-triangle.B;

	Vector crossed = Cross(&BA,&BC);
	Vector normal = NormalizeVector(crossed);
	
	double a = (triangle.A-ray.O)*normal; //(P0-P)*n
    double b = ray.D * normal; //d*n
    double t = a/b;
    
	if( t < 0 )return t;

    Vector point = ray.O+ray.D*t; //ray equation
	
	Vector AB = triangle.B-triangle.A;
	//Vector BC = triangle.C-triangle.B;
	Vector CA = triangle.A-triangle.C;

	Vector AP = point - triangle.A;
	Vector BP = point - triangle.B;
	Vector CP = point - triangle.C;

	double res1 = Cross(&AB,&AP)* normal;
	double res2 = Cross(&BC,&BP)* normal;
	double res3 = Cross(&CA,&CP)* normal;

	if((res1>=0 && res2 >=0 && res3 >=0) || (res1<=0 && res2 <=0 && res3 <=0)) {
		return t;
	}
	else
		return -1;

}


double calculate_t_plane(Ray ray,Plane plane){
    
    double a = (plane.P-ray.O)*plane.N; //(P0-P)*n
    double b = plane.N*ray.D; //d*n
    double t = a/b;
    
	if( t < 0 )return t;

    Vector point = ray.O+ray.D*t; //ray equation
	
	//now check whether the point intersectis the plane

    if( (plane.zmax==plane.zmin) && (point.x>=plane.xmin&&point.x<=plane.xmax) 
		&& (point.y>=plane.ymin&&point.y<=plane.ymax) ){
			return t;
    }
    else if( (plane.ymax==plane.ymin) &&  (point.x>=plane.xmin&&point.x<=plane.xmax) 
		&& (point.z>=plane.zmin&&point.z<=plane.zmax)){
			return t;
    }
    else if( (plane.xmax==plane.xmin) &&  (point.y>=plane.ymin&&point.y<=plane.ymax) 
		&& (point.z>=plane.zmin&&point.z<=plane.zmax)){
			return t;
    }
	else
		return -1;
}


double calculate_t_sphere(Ray ray,Sphere sphere){
    double A = getVectorLength(&ray.D) * getVectorLength(&ray.D);
    
    double B = (ray.O-sphere.C)*ray.D ;

    Vector u = ray.O-sphere.C;
    double C = getVectorLength(&u)*getVectorLength(&u)-sphere.R*sphere.R;

	double D = B*B-A*C;

    if( D < 0 )return -1;
    if( D == 0 )return -B/(A);

    double t1 = ( -B+sqrt(D) )/(A) ;
    double t2 = ( -B-sqrt(D) )/(A) ;

    if( t1<0 && t2 <0 )
		return -1;
    if( t1*t2 == 0 )
		return ( t1?t1:t2 );
    if( t1*t2 < 0 )
		return t1>t2 ? t1 : t2;
    
	return t1<t2?t1:t2;
}


double calculate_t_cylinder (Ray ray, Cylinder cylinder) {
	double t;
	double ox = ray.O.x - cylinder.xCenter;
	double oy = ray.O.y - cylinder.yCenter;
	double oz = ray.O.z ;

	double dx = ray.D.x;
    double dy = ray.D.y;
    double dz = ray.D.z;
        
    double a = dx * dx + dy * dy;   
    double b = 2.0 * (ox * dx + oy * dy);                                   
	double c = (ox * ox) + (oy * oy) - (cylinder.radius * cylinder.radius);
    
	double D = b * b - 4.0 * a * c ;
    if (D < 0.0){
        return -1;
    }
    else {
        
		double sqrtD = sqrt(D);
        double aa = a*2;
        double t = (-b-sqrtD) / aa;

        if (t > 0){
            double z = oz + t * dz;
			if (z > cylinder.zMin && z < cylinder.zMax){
                return t;
            }
        }

        t = (-b+sqrtD) / aa;
        if (t > 0){
            double z = oz + t * dz;
			if (z > cylinder.zMin && z < cylinder.zMax){
                    return t;
            }
         }
    }
    return -1;
}



Vector rayTrace(Ray ray,int depth){

    Vector pixeclColor;
    pixeclColor.x = 0;
	pixeclColor.y = 0;
	pixeclColor.z = 0;

    if(depth==0)
		return pixeclColor;

    Intersection mIntersection;
    Intersection closest;
    closest.t = -1;

    for(int i=0;i<PlaneList.size();i++){
        double current_t = calculate_t_plane(ray,PlaneList[i]);
        
		if( current_t < 0 )
			continue;
        
		mIntersection.t = current_t;
		mIntersection.obj_num = i;
		mIntersection.obj_type = "plane";
        
		if( closest.t == -1 || current_t <closest.t){
            closest = mIntersection;
        }
		
    }

    for(int i=0;i<SphereList.size();i++){
        double current_t = calculate_t_sphere(ray,SphereList[i]);
        
		if( current_t < 0 )
			continue;
        
		mIntersection.t = current_t;
		mIntersection.obj_num = i;
		mIntersection.obj_type = "sphere";
        
		if( closest.t == -1 || current_t < closest.t){
            closest = mIntersection;
        }
       
    }

	for(int i=0;i<TriangleList.size();i++){
        double current_t = calculate_t_triangle(ray,TriangleList[i]);
        
		if( current_t < 0 )
			continue;
        
		mIntersection.t = current_t;
		mIntersection.obj_num = i;
		mIntersection.obj_type = "triangle";
        
		if( closest.t == -1 || current_t < closest.t){
            closest = mIntersection;
        }
       
    }

	for(int i=0;i<CylinderList.size();i++){
        double current_t = calculate_t_cylinder(ray,CylinderList[i]);
		//if(current_t != -1) {cout <<"t:"<<current_t;}
		if( current_t < 0 )
			continue; // t= -1
        
		mIntersection.t = current_t;
		mIntersection.obj_num = i;
		mIntersection.obj_type = "cylinder";
        
		if( closest.t == -1 || current_t < closest.t){
            closest = mIntersection;
        }
       
    }

    if( closest.t < 0 )
		return pixeclColor;

    Ray ray_reflected;
    Ray ray_diffuse_light;
    Vector normal_surface;
    Coefficient coefficient;

	if( closest.obj_type == "plane" && closest.obj_num == 0 ){
        Vector P;
        P = ray.O + ray.D*closest.t;
       
		ray_diffuse_light.O = P;
        
		int xx = floor(P.x/10) ;
        int yy = floor(P.y/10) ;
       
		if( ( abs(xx)+abs(yy) )%2 == 1  ){
            pixeclColor.x = 1;
            pixeclColor.y = 1;
            pixeclColor.z = 1;
        }
       
		Plane mPlane = PlaneList[closest.obj_num];
        mPlane.N = NormalizeVector(mPlane.N);
        
		ray_reflected.D = ray.D - mPlane.N*( 2*(ray.D*mPlane.N) ) ;
        ray_reflected.O = P+ray_reflected.D*.001;

        coefficient.ambient = mPlane.ambient;
        coefficient.diffuse = mPlane.diffuse;
        coefficient.speculer = mPlane.speculer;
        coefficient.reflect = mPlane.reflect;
        coefficient.specExp = mPlane.specExp;
        
		normal_surface = mPlane.N;
    }
	else if( closest.obj_type == "plane" ){
        Vector P;
        P = ray.O+ray.D*closest.t;
        
		ray_diffuse_light.O = P;
		Plane mPlane = PlaneList[closest.obj_num];
        pixeclColor = mPlane.color;
        
		mPlane.N = NormalizeVector(mPlane.N);
        
		ray_reflected.D = ray.D - mPlane.N*( 2*(ray.D*mPlane.N) ) ;
        ray_reflected.O = P + ray_reflected.D*.001;
        
		coefficient.ambient = mPlane.ambient;
        coefficient.diffuse = mPlane.diffuse;
        coefficient.reflect = mPlane.reflect;
        coefficient.speculer= mPlane.speculer;
        coefficient.specExp = mPlane.specExp;
        
		normal_surface = mPlane.N;
    }
	else if( closest.obj_type == "sphere" ){
        Vector P;
        P = ray.O+ray.D*closest.t;
       
		ray_diffuse_light.O = P;
		Sphere tempSphere = SphereList[closest.obj_num];
        pixeclColor = tempSphere.color;
        
		Vector normal = NormalizeVector(P-tempSphere.C);
        
		ray_reflected.D = ray.D - normal*( 2*(ray.D*normal) ) ;
        ray_reflected.O=P+ray_reflected.D*.001;
        
		coefficient.ambient = tempSphere.ambient;
        coefficient.diffuse=tempSphere.diffuse;
        coefficient.reflect=tempSphere.reflect;
        coefficient.speculer=tempSphere.speculer;
        coefficient.specExp=tempSphere.specExp;
        
		normal_surface = normal;
    }

	else if( closest.obj_type == "triangle" ){
        Vector P;
        P = ray.O+ray.D*closest.t;
        ray_diffuse_light.O = P;
		
		Triangle mtriangle = TriangleList[closest.obj_num];
        pixeclColor = mtriangle.color;

		Vector BA = mtriangle.A-mtriangle.B;
		Vector BC =  mtriangle.C-mtriangle.B;

		Vector crossed = Cross(&BA,&BC);
		Vector normal = NormalizeVector(crossed);

        ray_reflected.D = ray.D - normal*( 2*(ray.D*normal) ) ;
        ray_reflected.O = P + ray_reflected.D*.001;
        
		coefficient.ambient = mtriangle.ambient;
        coefficient.diffuse = mtriangle.diffuse;
        coefficient.reflect = mtriangle.reflect;
        coefficient.speculer = mtriangle.speculer;
        coefficient.specExp = mtriangle.specExp;

		normal_surface = normal;
    }
	else if( closest.obj_type == "cylinder" ){
		//cout << "processing cylinder";
        Vector P;
        P = ray.O+ray.D*closest.t;
        ray_diffuse_light.O = P;
		Cylinder mCylinder = CylinderList[closest.obj_num];
        pixeclColor = mCylinder.color;
		
		Vector normal = getVector(((ray.O.x-mCylinder.xCenter)+(closest.t*ray.D.x))/mCylinder.radius,
			((ray.O.y-mCylinder.yCenter)+(closest.t*ray.D.y))/mCylinder.radius,0);
        
        ray_reflected.D = ray.D - normal*( 2*(ray.D*normal) ) ;
        ray_reflected.O = P+ray_reflected.D*.001;
        
		coefficient.ambient = mCylinder.ambient;
        coefficient.diffuse=mCylinder.diffuse;
        coefficient.reflect=mCylinder.reflect;
        coefficient.speculer=mCylinder.speculer;
        coefficient.specExp=mCylinder.specExp;
        
		normal_surface = normal;
    }

    Vector initialPixelColor = pixeclColor;

    if( depth == initialDepth ){
        
		double diffuse_total = 0;
		double specular_total = 0;
		
		Vector source;
        Vector nD;
        Vector R;
		Vector light_source_clr = getVector(1,1,1);
        Vector N = normal_surface;

        for(int i=0;i<lightSources.size();i++){
			
            source = lightSources[i]-ray_diffuse_light.O;
            nD = ray.D*-1;
               
            double diff_comp = (source * N)/( getVectorLength(&source)*getVectorLength(&nD) );
			if(diff_comp < 0)
				diff_comp = 0;
              
            R = source + nD;
            double spec_comp = (R*N)/( getVectorLength(&R) * getVectorLength(&N) );
               
			if(spec_comp < 0)
				spec_comp = 0;

            spec_comp = pow(spec_comp,(double)coefficient.specExp);
			diffuse_total += (diff_comp/lightSources.size());
            specular_total += (spec_comp/lightSources.size());
           
        }
        return initialPixelColor*coefficient.ambient 
			+ light_source_clr * coefficient.diffuse * diffuse_total 
			+ light_source_clr * coefficient.speculer * specular_total 
			+ rayTrace(ray_reflected,depth-1) * coefficient.reflect;
    }
    else {
        double a = ((coefficient.diffuse)/(coefficient.diffuse+coefficient.reflect));
       
        return initialPixelColor*a + rayTrace(ray_reflected,depth-1)*(1-a);
    }
}

void generateImage(){
	cout << "\nImage Drawing Started!";
	
    bitmap_image image(windowSize,windowSize);
    image.set_all_channels(0,0,0);

	Vector windowPosition = camera.Position;

    Vector dir = NormalizeVector(camera.viewDirection);
    Vector updir = NormalizeVector(camera.camUpVector);
    Vector rightdir = NormalizeVector(camera.camRightVector);
   
    
	double length = (windowSize*.001 )/ tan(40.0*PI/180.);
    Vector eyePosition = windowPosition + (dir)*(-length);

    for(int i=-windowSize/2; i<windowSize/2; i++){
        for(int j=-windowSize/2; j<windowSize/2; j++){
            
			Ray shootRay;
            
			Vector rayOrigin = windowPosition + updir*i*.002 + rightdir*j*.002 ;
            shootRay.O = rayOrigin;
            
			Vector rayDIrection = NormalizeVector( rayOrigin - eyePosition );
            shootRay.D = rayDIrection;

			Vector pixel_color = rayTrace(shootRay,maxDepth);
            
			int X = i+windowSize/2;
			int Y = j+windowSize/2;
            
			pixels[Y][windowSize-X] = pixel_color;
        }
    }
    for(int i=0;i<windowSize;i++){
        for(int j=0;j<windowSize;j++){
            image.set_pixel(i,j,255*pixels[i][j].x,255*pixels[i][j].y,255*pixels[i][j].z);
        }
    }
    image.save_image("output.bmp");
    cout<<"\nImage done!"<<endl;
}

void drawChessSection(double w,int cells){
    glNormal3f(0.0,0.0,1.0);
    for(int i=0;i<cells;i++){
        for(int j=0;j<cells;j++){
            if( (i+j)%2==0 )glColor3f(0,0,0);
            else glColor3f(1,1,1);
            double xx=i*w;
            double yy=j*w;
            glBegin(GL_QUADS);
                glVertex3f(xx,yy,0);
                glVertex3f(xx+w,yy,0);
                glVertex3f(xx+w,yy+w,0);
                glVertex3f(xx,yy+w,0);
            glEnd();
        }
    }
}


void drawChessBoard() {
	double width=10,cells=20;
	drawChessSection(width,cells);
	glPushMatrix();{
	    glTranslatef(-width*cells,0,0);
	    drawChessSection(width,cells);
	}glPopMatrix();
	glPushMatrix();{
	    glTranslatef(0,-width*cells,0);
	    drawChessSection(width,cells);
	}glPopMatrix();
	glPushMatrix();{
	    glTranslatef(-width*cells,-width*cells,0);
	    drawChessSection(width,cells);
	}glPopMatrix();
}


void display(){

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    camera.init();
	glMatrixMode(GL_MODELVIEW);

	drawChessBoard();
	
	for(int i=0;i<SphereList.size();i++) {
		glPushMatrix();{
			glColor3f(SphereList[i].color.x,SphereList[i].color.y,SphereList[i].color.z);
			glTranslatef(SphereList[i].C.x,SphereList[i].C.y,SphereList[i].C.z);
			glutSolidSphere(SphereList[i].R,24,30);
		}glPopMatrix();
	}

	for(int i=0;i<TriangleList.size();i++) {
		glPushMatrix();{
			glBegin(GL_TRIANGLES);
			glColor3f(TriangleList[i].color.x, TriangleList[i].color.y, TriangleList[i].color.z);
				glVertex3f(TriangleList[i].A.x, TriangleList[i].A.y, TriangleList[i].A.z);
				glVertex3f(TriangleList[i].B.x, TriangleList[i].B.y, TriangleList[i].B.z);
				glVertex3f(TriangleList[i].C.x, TriangleList[i].C.y, TriangleList[i].C.z);
			glEnd();
		}glPopMatrix();
	}

	for(int i=0;i<CylinderList.size();i++) {
		GLUquadricObj *quad = gluNewQuadric();

		glPushMatrix();{
			glTranslatef(CylinderList[i].xCenter,CylinderList[i].yCenter,CylinderList[i].zMin);
			glColor3f(CylinderList[i].color.x, CylinderList[i].color.y, CylinderList[i].color.z);
			gluCylinder(quad,CylinderList[i].radius,CylinderList[i].radius,(CylinderList[i].zMax-CylinderList[i].zMin),20,20);
		}glPopMatrix();
	}
	

	glutSwapBuffers();

}

void keyboardListener(unsigned char key, int x,int y){
	switch(key){
        case '1':
            camera.RotateY(3);
            break;
        case '2':
            camera.RotateY(-3);
            break;
        case '3':
            camera.RotateX(3);
            break;
        case '4':
            camera.RotateX(-3);
            break;
        case '5':
            camera.RotateZ(3);
            break;
        case '6':
            camera.RotateZ(-3);
            break;

		case 'i':
			generateImage();
            break;

        case 27:
            exit(0);

        default:
	        break;
	}
}

void specialKeyListener(int key, int x,int y){
	switch(key){
		case GLUT_KEY_DOWN:
		    camera.Forward(3);
			break;
		case GLUT_KEY_UP:
		    camera.Forward(-3);
			break;
		case GLUT_KEY_RIGHT:
		    camera.straff(3);
			break;
		case GLUT_KEY_LEFT:
		    camera.straff(-3);
			break;
		case GLUT_KEY_PAGE_UP:
		    camera.Upward(3);
			break;
		case GLUT_KEY_PAGE_DOWN:
		    camera.Upward(-3);
			break;
		case GLUT_KEY_INSERT:
			break;

		case GLUT_KEY_HOME:
			break;
		case GLUT_KEY_END:
			break;

		default:
			break;
	}
}

void mouse(int button, int state, int x, int y){
	switch(button){
		case GLUT_LEFT_BUTTON:
			if(state == GLUT_DOWN){
				//cout << "mouse left clicked";
				generateImage();
			}
			break;
		case GLUT_RIGHT_BUTTON:
			break;
		case GLUT_MIDDLE_BUTTON:
			break;
		default:
			break;
	}
}

void animation(){
	
	glutPostRedisplay();
}


void Initialize(){
	

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(80,1,1,10000.0);

}


bool advance() {
	string line;
	for(getline(fin, line), lineno += 1; fin.good() && (line.size() == 0 || line[0] == '#'); getline(fin, line), lineno += 1)
		; // Skip empty lines and comments;
	if(fin.good()) {
		buffer = line;
		return true;
	}
	return false;
}

stringstream line() {
	stringstream ss;
	ss << buffer;
	return ss;
}

void input() {
	cout<<"parsinc input file\n\n";
	fin = ifstream ("Spec.txt");
	stringstream ss;

	Sphere tempSphere;
	Triangle tempTriangle;
	Cylinder tempCylinder;

	lineno = 0;
	string tag;

	while(advance()) {
		ss = line();
		ss >> tag;
		if(tag == "recDepth") {
			ss >> maxDepth;
		}
		if(tag == "pixels") {
			ss >> windowSize;
		}
		if(tag == "light") {
			float x,y,z;
			ss >> x >> y >> z;
			lightSources.push_back( getVector(x,y,z) );
		}
		if(tag == "objStart") {
			string obj_type;
			ss >> obj_type;
			if(obj_type == "SPHERE") {
      
				while(advance()) {
					ss = line();
					ss >> tag;
					if(tag == "objEnd")
						break;
					if(tag == "center") {
						ss>>tempSphere.C.x>>tempSphere.C.y>>tempSphere.C.z;
					}
					if(tag == "radius") {
						ss >> tempSphere.R;
					}
					if(tag == "color") {
						ss >> tempSphere.color.x>>tempSphere.color.y>>tempSphere.color.z;
					}
					if(tag == "ambCoeff") {
						ss >> tempSphere.ambient;
					}
					if(tag == "difCoeff") {
						ss >> tempSphere.diffuse;
					}
					if(tag == "refCoeff") {
						ss >> tempSphere.reflect;
					}
					if(tag == "specCoeff") {
						ss >> tempSphere.speculer;
					}
					if(tag == "specExp") {
						ss >> tempSphere.specExp;
					}
				}

				 SphereList.push_back(tempSphere);

			}
			//end sphere parsing
			if(obj_type == "CYLINDER") {
      
				while(advance()) {
					ss = line();
					ss >> tag;
					if(tag == "objEnd")
						break;

					if(tag == "xCenter") {
						ss>>tempCylinder.xCenter;
					}
					if(tag == "yCenter") {
						ss>>tempCylinder.yCenter;
					}
					if(tag == "radius") {
						ss >> tempCylinder.radius;
					}
					if(tag == "zMin") {
						ss >> tempCylinder.zMin;
					}
					if(tag == "zMax") {
						ss >> tempCylinder.zMax;
					}
					if(tag == "color") {
						ss >> tempCylinder.color.x>>tempCylinder.color.y>>tempCylinder.color.z;
					}
					if(tag == "ambCoeff") {
						ss >> tempCylinder.ambient;
					}
					if(tag == "difCoeff") {
						ss >> tempCylinder.diffuse;
					}
					if(tag == "refCoeff") {
						ss >> tempCylinder.reflect;
					}
					if(tag == "specCoeff") {
						ss >> tempCylinder.speculer;
					}
					if(tag == "specExp") {
						ss >> tempCylinder.specExp;
					}
				}

				 CylinderList.push_back(tempCylinder);

			}
			//end of cylinder

			if(obj_type == "TRIANGLE") {
      
				while(advance()) {
					ss = line();
					ss >> tag;
					if(tag == "objEnd")
						break;

					if(tag == "a") {
						ss>>tempTriangle.A.x >> tempTriangle.A.y >> tempTriangle.A.z;
					}
					if(tag == "b") {
						ss>>tempTriangle.B.x >> tempTriangle.B.y >> tempTriangle.B.z;
					}
					if(tag == "c") {
						ss>>tempTriangle.C.x >> tempTriangle.C.y >> tempTriangle.C.z;
					}
					
					if(tag == "color") {
						ss >> tempTriangle.color.x>>tempTriangle.color.y>>tempTriangle.color.z;
					}
					if(tag == "ambCoeff") {
						ss >> tempTriangle.ambient;
					}
					if(tag == "difCoeff") {
						ss >> tempTriangle.diffuse;
					}
					if(tag == "refCoeff") {
						ss >> tempTriangle.reflect;
					}
					if(tag == "specCoeff") {
						ss >> tempTriangle.speculer;
					}
					if(tag == "specExp") {
						ss >> tempTriangle.specExp;
					}
					if(tag == "refractiveIndex") {
						ss >> tempTriangle.refrac_index;
					}
				}

				 TriangleList.push_back(tempTriangle);

			}

			if(obj_type == "CHECKERBOARD") {
				Plane mcheckerPlane;

				mcheckerPlane.P=getVector(0,0,0);
				mcheckerPlane.N=getVector(0,0,1);
				
				mcheckerPlane.xmin=-999999;
				mcheckerPlane.xmax= 999999;
				mcheckerPlane.ymin=-999999;
				mcheckerPlane.ymax= 999999;
				mcheckerPlane.zmin=0;
				mcheckerPlane.zmax=0;
				
				while(advance()) {
					ss = line();
					ss >> tag;
					if(tag == "objEnd")
						break;

					
					if(tag == "ambCoeff") {
						ss >> mcheckerPlane.ambient;
					}
					if(tag == "difCoeff") {
						ss >> mcheckerPlane.diffuse;
					}
					if(tag == "refCoeff") {
						ss >> mcheckerPlane.reflect;
					}
					if(tag == "specCoeff") {
						ss >> mcheckerPlane.speculer;
					}
					if(tag == "specExp") {
						ss >> mcheckerPlane.specExp;
					}
					
				}

				PlaneList.push_back( mcheckerPlane );

			}

		}
	}

}

int main(int argc, char **argv){
	glutInit(&argc,argv);
	
	input();
	
	glutInitWindowSize(windowSize,windowSize);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);
	
	glutCreateWindow("Ray Tracer Offline4");
	
	Initialize();
	
	glEnable(GL_DEPTH_TEST);
	
	glutDisplayFunc(display );
	glutIdleFunc(animation);
	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);
	glutMouseFunc(mouse);
	
	glutMainLoop();

	return 0;
}



