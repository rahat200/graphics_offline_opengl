recDepth 5
pixels 500

objStart CHECKERBOARD
colorOne 1.0 1.0 1.0
colorTwo 0.0 0.0 0.0
ambCoeff 0.1
difCoeff 0.5
refCoeff 0.4
specCoeff 1.0
specExp 0.0
objEnd

objStart SPHERE
center -20.0 -10.0 30.0
radius 10.0
color 1.0 0.0 0.0
ambCoeff 0.3
difCoeff 0.1
refCoeff 0.3
specCoeff 0.3
specExp 0.0
objEnd

objStart SPHERE
center 30.0 10.0 14.0
radius 10.0
color 0.0 0.0 1.0
ambCoeff 0.3
difCoeff 0.1
refCoeff 0.3
specCoeff 0.3
specExp 0.0
objEnd

objStart SPHERE
center -10.0 30.0 14.0
radius 10.0
color 0.0 1.0 1.0
ambCoeff 0.3
difCoeff 0.1
refCoeff 0.3
specCoeff 0.3
specExp 0.0
objEnd

objStart SPHERE
center 10.0 -30.0 14.0
radius 10.0
color 1.0 0.0 0.7
ambCoeff 0.3
difCoeff 0.1
refCoeff 0.3
specCoeff 0.3
specExp 0.0
objEnd

objStart SPHERE
center -20.0 -15.0 10.0
radius 10.0
color 0.0 1.0 0.5
ambCoeff 0.3
difCoeff 0.1
refCoeff 0.3
specCoeff 0.3
specExp 0.0
objEnd

objStart CYLINDER
xCenter 50.0
yCenter -20.0
radius 10.0
zMin 0.0
zMax 20.0
color 0.5 0.6 1.0
ambCoeff 0.4
difCoeff 0.2
refCoeff 0.2
specCoeff 0.2
specExp 0.0
objEnd

objStart TRIANGLE
a -40.0 10.0 5.0
b 40.0 10.0 5.0
c 0.0 10.0 50.0
color 0.7 1.0 0.2
ambCoeff 0.3
difCoeff 0.2
refCoeff 0.3
specCoeff 0.2
specExp 5.0
refractiveIndex 1.5
objEnd

light 70.0 70.0 70.0
light -70.0 70.0 -70.0
light -15.0 10.0 30.0
