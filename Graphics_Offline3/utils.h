#define NOMINMAX
#define _USE_MATH_DEFINES

#include<windows.h>
#include<iostream>
#include<GL/glut.h>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>
#include <limits>
#include <string>
#include <math.h>
#include <sstream>
#include "bitmap_image.hpp"

using namespace std;

#define SQR(x) (x*x)
#define PI 3.14159265
#define RAD (PI/180.0)
#define MAX_SIZE 1024


struct Vector
{
	GLfloat x,y,z;
};

Vector getVector ( GLfloat x, GLfloat y, GLfloat z );

class Camera{
    public:

	Vector viewDirection;
	Vector camRightVector;
	Vector camUpVector;
	Vector Position;

	GLfloat RotatedX, RotatedY, RotatedZ;

	Camera();
	void init ( void );

	void Forward ( GLfloat Distance );
	void Upward ( GLfloat Distance );
	void straff ( GLfloat Distance );

	void Move ( Vector Direction );
	void RotateX ( GLfloat Angle );
	void RotateY ( GLfloat Angle );
	void RotateZ ( GLfloat Angle );

	

};

Vector getVector ( GLfloat x, GLfloat y, GLfloat z ){
	Vector ret;
	ret.x = x;
	ret.y = y;
	ret.z = z;
	return ret;
}

Vector ReverseVector( Vector v){
	v.x = -v.x;
	v.y = -v.y;
	v.z = -v.z;
	return v;
}

GLfloat getVectorLength( Vector * v){
	return (GLfloat)(sqrt(SQR(v->x)
		+SQR(v->y)
		+SQR(v->z)));
}

Vector Cross (Vector * u, Vector * v){
	Vector res;
	res.x = u->y*v->z - u->z*v->y;
	res.y = u->z*v->x - u->x*v->z;
	res.z = u->x*v->y - u->y*v->x;
	return res;
}
float operator* (Vector v, Vector u){
	return v.x*u.x+v.y*u.y+v.z*u.z;
}

Vector NormalizeVector( Vector v){
	Vector res;
	float r = getVectorLength(&v);
	if (r == 0.0f) 
		return getVector(0.0f,0.0f,0.0f);
	res.x = v.x / r;
	res.y = v.y / r;
	res.z = v.z / r;
	return res;
}



Vector operator+ (Vector v, Vector u){
	Vector res;
	res.x = v.x+u.x;
	res.y = v.y+u.y;
	res.z = v.z+u.z;
	return res;
}
Vector operator- (Vector v, Vector u){
	Vector res;
	res.x = v.x-u.x;
	res.y = v.y-u.y;
	res.z = v.z-u.z;
	return res;
}

Vector operator* (Vector v, float r){
	Vector res;
	res.x = v.x*r;
	res.y = v.y*r;
	res.z = v.z*r;
	return res;
}



Camera::Camera(){
	Position = getVector (10,-50,40);
	RotatedX = RotatedY = RotatedZ = 0.0;
	viewDirection = getVector( 0,1,0);
	camRightVector = getVector (1.0, 0.0, 0.0);
	camUpVector = getVector (0.0, 0.0, 1.0);
	
}

void Camera::Move (Vector Direction){
	Position = Position + Direction;
}

void Camera::RotateX (GLfloat Angle){
	RotatedX += Angle;
	viewDirection = NormalizeVector(viewDirection*cos(Angle*RAD)+ camUpVector*sin(Angle*RAD));
	camUpVector = Cross(&viewDirection, &camRightVector)*-1;
}

void Camera::RotateY (GLfloat Angle){
	RotatedY += Angle;
	viewDirection = NormalizeVector(viewDirection*cos(Angle*RAD)- camRightVector*sin(Angle*RAD));
	camRightVector = Cross(&viewDirection, &camUpVector);
}

void Camera::RotateZ (GLfloat Angle){
	RotatedZ += Angle;
	camRightVector = NormalizeVector(camRightVector*cos(Angle*RAD)+ camUpVector*sin(Angle*RAD));
	camUpVector = Cross(&viewDirection, &camRightVector)*-1;
}
void Camera::Forward( GLfloat Distance ){
	Position = Position + (viewDirection*-Distance);
}

void Camera::straff ( GLfloat Distance ){
	Position = Position + (camRightVector*Distance);
}

void Camera::Upward( GLfloat Distance ){
	Position = Position + (camUpVector*Distance);
}
void Camera::init( void ){
	Vector ViewPoint = Position + viewDirection;
	gluLookAt(	Position.x,Position.y,Position.z,
				ViewPoint.x,ViewPoint.y,ViewPoint.z,
				camUpVector.x,camUpVector.y,camUpVector.z);

}


